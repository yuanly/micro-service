package com.module.admin.ms.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.module.admin.ms.pojo.MsUser;
import com.system.comm.model.KvEntity;

/**
 * ms_user的Dao
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
public interface MsUserDao {

	public abstract void save(MsUser msUser);

	public abstract void update(MsUser msUser);

	public abstract void delete(@Param("muId")String muId);

	public abstract MsUser get(@Param("muId")String muId);

	public abstract List<MsUser> findMsUser(MsUser msUser);
	
	public abstract int findMsUserCount(MsUser msUser);

	public abstract List<KvEntity> findSearch(@Param("searchString")String searchString);
}