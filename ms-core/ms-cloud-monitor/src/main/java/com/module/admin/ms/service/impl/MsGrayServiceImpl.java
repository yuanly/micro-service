package com.module.admin.ms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.ms.dao.MsGrayDao;
import com.module.admin.ms.pojo.MsGray;
import com.module.admin.ms.service.MsGrayService;
import com.system.comm.enums.Boolean;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseFrame;
import com.system.handle.model.ResponseCode;

/**
 * ms_gray的Service
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Component
public class MsGrayServiceImpl implements MsGrayService {

	@Autowired
	private MsGrayDao msGrayDao;
	
	@Override
	public ResponseFrame saveOrUpdate(MsGray msGray) {
		ResponseFrame frame = new ResponseFrame();
		if(FrameStringUtil.isEmpty(msGray.getMgId())) {
			msGrayDao.save(msGray);
		} else {
			msGrayDao.update(msGray);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MsGray get(String mgId) {
		return msGrayDao.get(mgId);
	}

	@Override
	public ResponseFrame pageQuery(MsGray msGray) {
		msGray.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = msGrayDao.findMsGrayCount(msGray);
		List<MsGray> data = null;
		if(total > 0) {
			data = msGrayDao.findMsGray(msGray);
			for (MsGray mg : data) {
				mg.setIsUseName(Boolean.getText(mg.getIsUse()));
			}
		}
		Page<MsGray> page = new Page<MsGray>(msGray.getPage(), msGray.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String mgId) {
		ResponseFrame frame = new ResponseFrame();
		msGrayDao.delete(mgId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<MsGray> findUse() {
		return msGrayDao.findUse();
	}
}