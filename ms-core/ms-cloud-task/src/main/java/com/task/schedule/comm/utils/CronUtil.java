package com.task.schedule.comm.utils;

import java.text.ParseException;
import java.util.Date;

import org.quartz.CronExpression;

/**
 * 定时任务的表达式工具类
 * @author yuejing
 * @date 2019年3月29日 下午3:00:39
 */
public class CronUtil {

	/**
	 * 根据表达式获取下次执行的时间
	 * @param cron
	 * @param curDate
	 * @return
	 */
	public static Date getNextValidTime(String cron, Date curDate) {
		try {
			CronExpression cronExpression = new CronExpression(cron);
			Date nextValidTime = cronExpression.getNextValidTimeAfter(curDate);
			return nextValidTime;
		} catch (ParseException e) {
			return null;
		}
	}
}
