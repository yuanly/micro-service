<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑灰度服务</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
		<input type="hidden" id="mgId" value="${msGray.mgId}">
  		<div class="form-group">
			<label for="normalServiceId" class="col-sm-4">正常服务 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="normalServiceId" placeholder="正常服务的编码" value="${msGray.normalServiceId}"></div>
		</div>
  		<div class="form-group">
			<label for="grayServiceId" class="col-sm-4">灰度服务 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="grayServiceId" placeholder="灰度服务的编码" value="${msGray.grayServiceId}"></div>
		</div>
		<div class="form-group">
			<label for="isUse" class="col-sm-4">使用 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:radio id="isUse" name="isUse" dictcode="boolean" value="${msGray.isUse}" defvalue="1" /></div>
		</div>
  		<%-- <div class="form-group">
			<label for="useTime" class="col-sm-4">生效时间 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<input size="16" type="text" class="form-control" id="useTime" placeholder="请选择发布时间" value="<my:date value="${msGray.useTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})">
			</div>
		</div> --%>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/my97date.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var saveMsg = $('#saveMsg').empty();
			var normalServiceId = $('#normalServiceId');
			if(JUtil.isEmpty(normalServiceId.val())) {
				saveMsg.append('请输入正常服务的编码');
				normalServiceId.focus();
				return;
			}
			var grayServiceId = $('#grayServiceId');
			if(JUtil.isEmpty(grayServiceId.val())) {
				saveMsg.append('请输入灰度服务的编码');
				grayServiceId.focus();
				return;
			}
			
			JUtil.ajax({
				eventId: 'saveBtn',
				url: '${webroot}/msGray/f-json/save.shtml',
				data: {
					mgId: $('#mgId').val(),
					normalServiceId: normalServiceId.val(),
					grayServiceId: grayServiceId.val(),
					isUse: $('input[name="isUse"]:checked').val()
				},
				success : function(json) {
					if (json.code === 0) {
						saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						saveMsg.append(JUtil.msg.ajaxErr);
					else
						saveMsg.append(json.message);
				}
			});
		});
	});
	</script>
</body>
</html>