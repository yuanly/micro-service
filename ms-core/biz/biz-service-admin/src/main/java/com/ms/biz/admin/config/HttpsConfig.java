package com.ms.biz.admin.config;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.ms.env.Env;

@Configuration
public class HttpsConfig {

	@Autowired
	private Environment env;

	/*@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
		tomcat.addAdditionalTomcatConnectors(createHttpsConnector());
		return tomcat;
	}*/

	@Bean
	//@ConditionalOnProperty(name="condition.http2https", havingValue="true", matchIfMissing=false)
	public ServletWebServerFactory servletContainer() {
		TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
		/* {
				@Override
				protected void postProcessContext(Context context) {
					SecurityConstraint constraint = new SecurityConstraint();
					constraint.setUserConstraint("CONFIDENTIAL");
					SecurityCollection collection = new SecurityCollection();
					collection.addPattern("/*");
					constraint.addCollection(collection);
					context.addConstraint(constraint);
				}
			}*/
		tomcat.addAdditionalTomcatConnectors(createHttpConnector());
		return tomcat;
	}

	//@ConditionalOnProperty(name="condition.http2https", havingValue="true", matchIfMissing=false)
	private Connector createHttpConnector() {
		Integer port = Integer.valueOf(env.getProperty(Env.HTTP_PORT.getCode()));
		/*String keystoreFile = env.getProperty(Env.HTTPS_KEYSSTORE.getCode());
		String keystorePass = env.getProperty(Env.HTTPS_KEYSSTORE_PASSWORD.getCode());
		String keyAlias = env.getProperty(Env.HTTPS_KEY_ALIAS.getCode());
		String ciphers = env.getProperty(Env.HTTPS_CIPHERS.getCode());*/


		Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		//Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
		//File keystore = new ClassPathResource("sample.jks").getFile();
		/*File truststore = new ClassPathResource("sample.jks").getFile();*/
		//connector.setScheme("http");
		//connector.setSecure(false);
		connector.setPort(port);
		/*protocol.setSSLEnabled(true);
		protocol.setKeystoreFile(keystoreFile);
		protocol.setKeystorePass(keystorePass);
		protocol.setKeyPass(keystorePass);
		if(FrameStringUtil.isNotEmpty(keyAlias)) {
			protocol.setKeyAlias(keyAlias);
		}
		if(FrameStringUtil.isNotEmpty(ciphers)) {
			protocol.setCiphers(ciphers);
		}*/
		return connector;


		/*Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		connector.setScheme("https");
		connector.setPort(port);
		connector.setSecure(true);
		connector.setProperty("SSLEnabled", "true");
		connector.setProperty("keystoreFile", keystoreFile);
		connector.setProperty("keystorePass", keystorePass);
		if(FrameStringUtil.isNotEmpty(keyAlias)) {
			connector.setProperty("keyAlias", keyAlias);
		}
		if(FrameStringUtil.isNotEmpty(ciphers)) {
			connector.setProperty("ciphers", ciphers);
		}
		return connector;*/
	}

}