<%@page import="com.module.admin.prj.enums.PrjInfoContainer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查看系统优化的详细信息</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
		<div class="form-group">
			<label for="prjId" class="col-sm-4">项目</label>
			<div class="col-sm-8">${prjOptimizeLog.prjName}</div>
		</div>
  		<div class="form-group">
			<label for="url" class="col-sm-4">地址</label>
			<div class="col-sm-8">${prjOptimizeLog.url}</div>
		</div>
  		<div class="form-group">
			<label for="url" class="col-sm-4">请求时间</label>
			<div class="col-sm-8"><my:date value="${prjOptimizeLog.reqTime}"/></div>
		</div>
  		<div class="form-group">
			<label for="url" class="col-sm-4">响应时间</label>
			<div class="col-sm-8"><my:date value="${prjOptimizeLog.resTime}"/></div>
		</div>
  		<div class="form-group">
			<label for="url" class="col-sm-4">所用时间</label>
			<div class="col-sm-8">${prjOptimizeLog.useTime}ms</div>
		</div>
  		<div class="form-group">
			<label for="url" class="col-sm-4">响应大小</label>
			<div class="col-sm-8">${prjOptimizeLog.resSize}kb</div>
		</div>
  		<div class="form-group">
			<label for="url" class="col-sm-4">备注</label>
			<div class="col-sm-8">${prjOptimizeLog.remark}</div>
		</div>
		<hr />
  		<div class="form-group">
			<label for="params" class="col-sm-4">请求参数</label>
			<div class="col-sm-8 text-success"><small id="params" onclick="JUtil.selectText('params')">${prjOptimizeLog.params}</small></div>
		</div>
  		<div class="form-group">
			<label for="resResult" class="col-sm-4">响应结果</label>
			<div class="col-sm-8 text-success"><small id="resResult" onclick="JUtil.selectText('resResult')">${prjOptimizeLog.resResult}</small></div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	var info = {
	};
	$(function() {
	});
	</script>
</body>
</html>