package com.module.admin.ms.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * ms_gray实体
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Alias("msGray")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MsGray extends BaseEntity implements Serializable {
	//编号
	private String mgId;
	//正常服务ID
	private String normalServiceId;
	//灰度服务ID
	private String grayServiceId;
	//是否使用
	private Integer isUse;
	//创建时间
	private Date createTime;
	//================= Extended attribute
	// 灰度用户列表
	private List<MsGrayUser> users;
	//是否使用名称
	private String isUseName;
	
	public String getMgId() {
		return mgId;
	}
	public void setMgId(String mgId) {
		this.mgId = mgId;
	}
	
	public String getNormalServiceId() {
		return normalServiceId;
	}
	public void setNormalServiceId(String normalServiceId) {
		this.normalServiceId = normalServiceId;
	}
	
	public String getGrayServiceId() {
		return grayServiceId;
	}
	public void setGrayServiceId(String grayServiceId) {
		this.grayServiceId = grayServiceId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public List<MsGrayUser> getUsers() {
		return users;
	}
	public void setUsers(List<MsGrayUser> users) {
		this.users = users;
	}
	public Integer getIsUse() {
		return isUse;
	}
	public void setIsUse(Integer isUse) {
		this.isUse = isUse;
	}
	public String getIsUseName() {
		return isUseName;
	}
	public void setIsUseName(String isUseName) {
		this.isUseName = isUseName;
	}
}