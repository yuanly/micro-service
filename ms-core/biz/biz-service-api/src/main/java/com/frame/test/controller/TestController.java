package com.frame.test.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.frame.test.pojo.Test;
import com.frame.test.service.TestService;
import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.system.comm.utils.FrameJsonUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * test接口
 * @author yuejing
 * @date 2016年1月29日 下午9:31:59
 * @version V1.0.0
 */
@RestController
public class TestController {

    private final Logger LOGGER = LoggerFactory.getLogger(TestController.class);
    
	@Autowired
	private TestService testService;

	/**
	 * 获取对象
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/test/get")
	@ApiInfo(params = {
			@ApiParam(name="编号", code="id", value="23"),
		}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=List.class, value="")
	})
	public ResponseFrame get(HttpServletRequest request, String id) {
		try {
			ResponseFrame frame = new ResponseFrame();
			Test data = testService.get(id);
			LOGGER.info("id[" + id + "]获取数据信息: " + FrameJsonUtil.toString(data));
			frame.setBody(data);
			frame.setCode(ResponseCode.SUCC.getCode());
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/test/findAll")
	@ApiInfo(params = {
		}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=List.class, value="")
	})
	public ResponseFrame findAll() {
		try {
			ResponseFrame frame = new ResponseFrame();
			List<Test> data = testService.findAll();
			frame.setBody(data);
			frame.setCode(ResponseCode.SUCC.getCode());
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/test/save")
	public ResponseFrame save(Test test) {
		try {
			ResponseFrame frame = testService.save(test);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}
}