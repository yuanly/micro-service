package com.module.admin.prj.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.admin.BaseController;
import com.module.admin.prj.pojo.PrjApiTest;
import com.module.admin.prj.service.PrjApiTestService;
import com.module.admin.prj.service.PrjInfoService;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * prj_api测试的Controller
 * @author yuejing
 * @date 2016-11-30 13:30:00
 * @version V1.0.0
 */
@Controller
public class PrjApiTestController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrjApiTestController.class);

	@Autowired
	private PrjApiTestService prjApiTestService;
	@Autowired
	private PrjInfoService prjInfoService;

	/**
	 * 跳转到管理页
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/prjApiTest/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		List<KvEntity> prjInfos = prjInfoService.findKvAll();
		modelMap.put("prjInfos", prjInfos);
		return "admin/prj/apiTest-manager";
	}

	@RequestMapping(value = "/prjApiTest/f-json/get")
	@ResponseBody
	public void get(HttpServletRequest request, HttpServletResponse response,
			Integer id) {
		ResponseFrame frame = new ResponseFrame();
		try {
			PrjApiTest data = prjApiTestService.get(id);
			frame.setBody(data);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/prjApiTest/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			PrjApiTest prjApiTest) {
		ResponseFrame frame = null;
		try {
			frame = prjApiTestService.pageQuery(prjApiTest);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/prjApiTest/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap,
			Integer id, Integer prjId) {
		if(id != null && prjId != null) {
			modelMap.put("prjApiTest", prjApiTestService.get(id));
		}
		List<KvEntity> prjInfos = prjInfoService.findKvAll();
		modelMap.put("prjInfos", prjInfos);
		return "admin/prj/apiTest-edit";
	}
	
	@RequestMapping(value = "/prjApiTest/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response,
			PrjApiTest prjApiTest) {
		ResponseFrame frame = null;
		try {
			frame = prjApiTestService.saveOrUpdate(prjApiTest);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/prjApiTest/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response,
			Integer id) {
		ResponseFrame frame = null;
		try {
			frame = prjApiTestService.delete(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/prjApiTest/f-json/test")
	@ResponseBody
	public void test(HttpServletRequest request, HttpServletResponse response,
			Integer id) {
		ResponseFrame frame = null;
		try {
			frame = prjApiTestService.test(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}
}