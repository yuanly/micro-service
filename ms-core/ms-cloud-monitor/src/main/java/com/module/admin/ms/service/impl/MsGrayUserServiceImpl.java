package com.module.admin.ms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.ms.dao.MsGrayUserDao;
import com.module.admin.ms.enums.MsGrayUserType;
import com.module.admin.ms.pojo.MsGrayUser;
import com.module.admin.ms.pojo.MsUser;
import com.module.admin.ms.service.MsGrayUserService;
import com.module.admin.ms.service.MsUserService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * ms_gray_user的Service
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Component
public class MsGrayUserServiceImpl implements MsGrayUserService {

	@Autowired
	private MsGrayUserDao msGrayUserDao;
	@Autowired
	private MsUserService msUserService;
	
	@Override
	public ResponseFrame save(MsGrayUser msGrayUser) {
		ResponseFrame frame = new ResponseFrame();
		if (FrameStringUtil.isEmpty(msGrayUser.getType())) {
			msGrayUser.setType(MsGrayUserType.MS_USER.getCode());
		}
		MsGrayUser org = get(msGrayUser.getMgId(), msGrayUser.getType(), msGrayUser.getTypeValue());
		if(org == null) {
			msGrayUserDao.save(msGrayUser);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MsGrayUser get(String mgId, String type, String typeValue) {
		return msGrayUserDao.get(mgId, type, typeValue);
	}

	@Override
	public ResponseFrame pageQuery(MsGrayUser msGrayUser) {
		msGrayUser.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = msGrayUserDao.findMsGrayUserCount(msGrayUser);
		List<MsGrayUser> data = null;
		if(total > 0) {
			data = msGrayUserDao.findMsGrayUser(msGrayUser);
			for (MsGrayUser mgu : data) {
				if (MsGrayUserType.MS_USER.getCode().equals(mgu.getType())) {
					// 具体用户
					MsUser mu = msUserService.get(mgu.getTypeValue());
					if (mu != null) {
						mgu.setName(mu.getName());
						mgu.setPhone(mu.getPhone());
						mgu.setRemark(mu.getRemark());
					}
				}
				mgu.setTypeName(MsGrayUserType.getText(mgu.getType()));
			}
		}
		Page<MsGrayUser> page = new Page<MsGrayUser>(msGrayUser.getPage(), msGrayUser.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String mgId, String type, String typeValue) {
		ResponseFrame frame = new ResponseFrame();
		msGrayUserDao.delete(mgId, type, typeValue);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<MsGrayUser> findByMgId(String mgId) {
		return msGrayUserDao.findByMgId(mgId);
	}
}