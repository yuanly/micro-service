package com.module.admin.ms.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.system.comm.model.KvEntity;

/**
 * 灰度服务用户类型
 * @author yuejing
 * @date 2019年4月3日 上午10:20:15
 */
public enum MsGrayUserType {
	/** 具体用户 */
	MS_USER		("10", "用户"),
	/** 其它 */
	//OTHER		("20", "其它"),
	;
	
	public static final String KEY = "ms_gray_user_type";

	private String code;
	private String name;
	private static List<KvEntity> list = new ArrayList<KvEntity>();
	private static Map<String, String> map = new HashMap<String, String>();

	private MsGrayUserType(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	static {
		EnumSet<MsGrayUserType> set = EnumSet.allOf(MsGrayUserType.class);
		for(MsGrayUserType e : set){
			map.put(e.getCode(), e.getName());
			list.add(new KvEntity(e.getCode(), e.getName()));
		}
	}

	/**
	 * 根据Code获取对应的汉字
	 * @param code
	 * @return
	 */
	public static String getText(String code) {
		return map.get(code);
	}
	
	/**
	 * 获取集合
	 * @return
	 */
	public static List<KvEntity> getList() {
		return list;
	}

	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
}
