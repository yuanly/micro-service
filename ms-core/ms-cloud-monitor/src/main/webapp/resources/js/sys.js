JUtil.sys = {
		//跳转到指定地址
		location : function(url, islogin, isback) {
			if(islogin && user.uname==='') { JUtil.open.login(); return; }
			if(isback) {
				if(url.indexOf('?')!=-1) url += '&';
				else url += '?';
				url += 'back='+location;
			}
			location = url;
		},
		//初始化系统资料
		init : function() {
			if(config.initHmId)
				JUtil.sys.initHeader(config.initHmId);
		},
		//初始化header的选中
		initHeader : function(id) {
			$('#headerMenu>ul>li').each(function(i, obj) {
				if($(obj).attr('id') == id) {
					$(obj).addClass('active');
					return true;
				}
			});
		},
		initOptMore : function() {
			$('.opt-more').mouseover(function() {   
			    $(this).addClass('open');
			}).mouseout(function() {
				$(this).removeClass('open');
			});
		},
		/*
		 * 下拉搜索框
		 * 实例：JUtil.sys.typeahead({
					id: 'msUserName',
					url: webroot + '/msUser/f-json/findSearch.shtml',
					showIndex: true,
					valueId: 'typeValue'
				});
		 * id: 控件ID
		 * url: 搜索地址
		 * showIndex: 是否显示索引数 true/false
		 * valueId: 具体值的控件ID
		 */
		typeahead: function(attr) {
			$('#' + attr.id).typeahead({
	        	//数据源
	            //source: dataSource,
	            source: function (query, process) {
	                //query是输入的值
                	if(attr.showIndex!=undefined && attr.showIndex) {
                		var len = query.indexOf('.');
                		if(len != -1) {
                			query = query.substring(len + 2);
                		}
                	}
                	if(attr.data == undefined) {
                		attr.params = { searchString: query };
                	}
	                return $.ajax({url: attr.url,
	                		data: attr.params,
	                		type: 'post',
	                		dataType: 'json',
	                		success: function (json) {
	                    if (json.code === 0) {
	                        var array = [];
	                        var value = undefined;
	                    	if(attr.array === undefined) {
	                    		attr.array = {};
	                    	}
	                    	if(attr.showIndex!=undefined && attr.showIndex) {
    	                        $.each(json.body, function (i, obj) {
		                        	if(attr.value) {
		                        		obj.value = obj[attr.value];
		                        		obj.code = obj[attr.code];
		                        	}
    	                        	value = (i + 1) + '. ' + obj.value;
    	                            array.push(value);
    	                            attr.array[value] = obj;
    	                        });
	                    	} else {
		                        $.each(json.body, function (i, obj) {
		                        	if(attr.value) {
		                        		obj.value = obj[attr.value];
		                        		obj.code = obj[attr.code];
		                        	}
		                    		value = obj.value;
		                            array.push(value);
    	                            attr.array[value] = obj;
		                        });
	                    	}
	                        process(array);
	                    }
	                }});
	            },
	            showHintOnFocus: 'all',
	            autoSelect: false,
	            afterSelect: function (item) {
	            	//选择项之后的事件，item是当前选中的选项
	            	if(attr.valueId != undefined) {
	            		$('#' + attr.valueId).val(attr.array[item].code);
	            	}
	            	if(attr.showIndex!=undefined && attr.showIndex) {
	            		var len = item.indexOf('.');
                		if(len != -1) {
                			item = item.substring(len + 2);
                		}
	            		$('#' + attr.id).val(item);
					}
	            }
	        });
			
			$('#' + attr.id).blur(function() {
				if(attr.array == undefined) {
					return;
				}
				if(attr.valueId != undefined) {
					var value = $('#' + attr.id).val();
					var isClear = true;
					$.each(attr.array, function(key, val) {
						var prefix = '';
						if(attr.showIndex == true) {
							prefix = '. ';
						}
						if(key.indexOf(prefix + value)!=-1) {
							isClear = false;
							return false;
						}
					});
					if(isClear) {
						$('#' + attr.valueId).val('');
					}
				}
				if(attr.onlyChoose == true) {
					var value = $('#' + attr.id).val();
					var isClear = true;
					$.each(attr.array, function(key, val) {
						var newKey = key;
						if(attr.showIndex == true) {
							newKey = key.substr(key.indexOf('. ') + 2);
						}
						if(newKey == value) {
							isClear = false;
							return false;
						}
					});
					if(isClear) {
						$('#' + attr.id).val('');
						$('#' + attr.valueId).val('');
					}
				}
			});
		}
};


$(function() {
	JUtil.sys.initOptMore();
});