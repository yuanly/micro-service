package com.system.comm.config;

import com.system.comm.utils.FrameStringUtil;

/**
 * 系统的配置文件
 * @author yuejing
 *
 */
public class SystemConfig {

	/**
	 * 系统的版本号
	 */
	private static String version;

	/**
	 * 初始化系统参数
	 * @param version
	 */
	public static void init(String version) {
		SystemConfig.version = version;
	}

	public static String getVersion() {
		if(FrameStringUtil.isEmpty(version)) {
			version = "1.0.0";
		}
		return version;
	}
	public static void setVersion(String version) {
		SystemConfig.version = version;
	}
}