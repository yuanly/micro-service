<%@page import="com.module.admin.sys.enums.SysFileType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/inc/sys.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-ANT</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp">
			<jsp:param name="first" value="prj"/>
			<jsp:param name="second" value="prjAntManager"/>
		</jsp:include>
		<div class="c-right">
			<div class="${bodyMainClass}">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">项目 / <b>原型管理</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<a href="javascript:history.go(-1)" class="btn btn-default btn-sm">返回</a>
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
				  	<div class="row">
				  		<div class="col-sm-3">
				  			<div class="table-tool-panel">
				  			<div class="row">
								<div class="col-sm-6">
								</div>
								<div class="col-sm-6 text-right">
								  	<div class="btn-group">
								  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增功能</a>
								  	</div>
								</div>
							</div>
							</div>
							<div id="infoPanel" class="table-panel"></div>
							<div id="infoPage" class="table-page-panel"></div>
				  		</div>
				  		<div class="col-sm-9">
				  			<div class="table-tool-panel" style="min-height: 440px;" id="rightPanel">
							</div>
				  		</div>
				  	</div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/upload.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		//记录信息
		list : {},
		//获取用户信息
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                        '<th>功能名称</th>',
				                        '<th width="100">操作</th>',
				                        '</tr></thead><tbody>'].join('');
				infoPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				infoPage.page = page;

			JUtil.ajax({
				url : '${webroot}/prjAnt/f-json/pageQuery.shtml',
				data : { page:infoPage.page, size:infoPage.size,
					prjId:$('#prjId').val(), pid: '${prjAnt.paId}'
				},
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							info.list[obj.paId] = obj;
							return ['<tr>',
							    	'<td>',obj.name,'</td>',
							    	//'<td style="word-wrap:break-word;word-break:break-all;">',obj.method,'</td>',
							    	'<td><a class="glyphicon glyphicon-edit text-success" href="javascript:info.edit(',obj.paId,')" title="修改"></a>',
							    	'&nbsp; <a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(',obj.paId,')" title="删除"></a>',
							    	'&nbsp; &nbsp; <a class="glyphicon text-success" href="javascript:info.look(',obj.paId,')" title="查看">查看</a>',
							    	'</td>',
								'</tr>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		look : function(paId) {
			var obj = info.list[paId];
			$('#rightPanel').empty().append(['<div class="enter-panel">',
										  		'	<div class="form-group">',
												'		名称：<span class="label label-warning">',obj.name,'</span>',
												'	</div>',
												'	<div class="form-group">',
												'		<img id="filesLoading" alt="原型图" src="${webroot}/file/f-view/preview.shtml?url=',obj.img,'"/>',
												'	</div>',
												'	<div class="form-group">',
												'		<div id="apiTextPanel"></div>',
												'	</div>',
												'</div>'].join(''));
			if(obj.apiText != '') {
				$.each(eval('(' + obj.apiText + ')'), function(i, cld) {
					info.addApiDtl(cld.code, cld.url, true);
				});
			}
		},
		edit : function(paId) {
			if(paId === undefined) {
				paId = '';
			}
			var name = info.list[paId] == undefined ? '' : info.list[paId].name;
			var img = info.list[paId] == undefined ? '' : info.list[paId].img;
			var apiText = info.list[paId] == undefined ? '' : info.list[paId].apiText;
			$('#rightPanel').empty().append(['<div class="enter-panel">',
									'	<input type="hidden" id="paId" value="',paId,'">',
							  		'	<input type="hidden" id="prjId" value="${param.prjId}">',
							  		'	<div class="form-group">',
									'		<input type="text" class="form-control input-sm" id="name" placeholder="名称" value="',name,'">',
									'	</div>',
									'	<div class="form-group row">',
									'		<label for="img" class="col-sm-4">原型图片上传 <span class="text-danger">*</span></label>',
									'		<div class="col-sm-8">',
									'			<input type="file" id="files" name="files" onchange="info.uploadFile()"/><img id="filesLoading" alt="上传中..." src="${webroot}/resources/images/loading.gif" style="display: none;"/>',
									'			<input type="hidden" id="imgUrl" name="imgUrl" value="',img,'"/>',
									'		</div>',
									'	</div>',
									'	<div class="form-group"><img id="imgFile" alt="原型图" src="${webroot}/resources/images/no_img.jpg"/></div>',
									'	<div class="form-group">',
									'		<input type="text" class="form-control input-sm" id="code" placeholder="编号" value="" style="width: 60px;display: inline-block;">',
									'		<input type="text" class="form-control input-sm" id="apiLink" placeholder="API链接地址" value="" style="width: 400px;display: inline-block;">',
									'		<a href="javascript:;" class="btn btn-sm btn-default" onclick="info.addApi()">添加</a>',
									'		<div id="apiTextPanel"></div>',
									'	</div>',
									'	<hr/>',
								  	'	<div class="form-group text-right">',
									'		<span id="saveMsg" class="label label-danger"></span>',
								 	'		<div class="btn-group">',
									'			<button type="button" onclick="info.save()" id="saveBtn" class="btn btn-success enter-fn">保存</button>',
									'		</div>',
									'	</div>',
									'</div>'].join(''));
			if(apiText != '') {
				$.each(eval('(' + apiText + ')'), function(i, obj) {
					info.addApiDtl(obj.code, obj.url);
				});
			}
			if(img != '') {
				$('#imgFile').attr('src', '${webroot}/file/f-view/preview.shtml?url='+img);
			}
		},
		addApi : function() {
			var code = $('#code');
			if(JUtil.isEmpty(code.val())) {
				code.focus();
				return;
			}
			var apiLink = $('#apiLink');
			if(JUtil.isEmpty(apiLink.val())) {
				apiLink.focus();
				return;
			}
			info.addApiDtl(code.val(), apiLink.val());
			code.val('');
			apiLink.val('');
		},
		addApiDtl : function(code, apiLink, hiddenDel) {
			$('#apiTextPanel').append(['<hr/><div><small class="label label-warning">',code,'</small>',
			                           '&nbsp; &nbsp; <small><a href="',apiLink,'" class="text-success" target="_blank">',apiLink,'</a></small> &nbsp;',
			                           (hiddenDel!=undefined?'':'<a class="glyphicon glyphicon-remove text-success" href="javascript:;" onclick="$(this).parent().remove()" title="删除"></a>'),
			                           '<input type="hidden" name="apiText" value="',code,';',apiLink,'">',
			                           '</div>'].join(''));
		},
		uploadFile : function() {
			upload.file({
				fileId : 'files',
				loading : 'filesLoading',
				url : '${webroot}/sysFile/f-json/upload.shtml',
				param : {
					filename : 'files', type : '<%=SysFileType.PRJ_ANT.getCode()%>'
				},
				success : function(data) {
					$('#imgUrl').val(data.body.url);
					$('#imgFile').attr('src', '${webroot}/file/f-view/preview.shtml?url='+data.body.url);
				}
			});
		},
		save : function() {
			var _saveMsg = $('#saveMsg').empty();
			_saveMsg.attr('class', 'label label-danger');
			var paId = $('#paId').val();
			var prjId = $('#prjId').val();
			var name = $('#name');
			if(JUtil.isEmpty(name.val())) {
				_saveMsg.append('请输入名称');
				name.focus();
				return;
			}
			var img = $('#imgUrl');
			if(JUtil.isEmpty(img.val())) {
				_saveMsg.append('请上传图片');
				return;
			}
			var apiText = '[';
			$('input[name="apiText"]').each(function(i, obj) {
				var arr = $(obj).val().split(';');
				apiText += '{"code":"' + arr[0] + '", "url":"' + arr[1] + '"},';
			});
			if(apiText != '[') {
				apiText = apiText.substring(0, apiText.length - 1);
			}
			apiText += ']';
			if(apiText === '[]') {
				_saveMsg.append('请添加API的信息');
				return;
			}
			
			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/prjAnt/f-json/save.shtml',
				data : {
					paId: paId,
					prjId: prjId,
					name: name.val(),
					pid: '${prjAnt.paId}',
					img: img.val(),
					apiText: apiText
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		},
		del : function(id) {
			if(confirm('您确定要删除该记录吗?')) {
				JUtil.ajax({
					url : '${webroot}/prjAnt/f-json/delete.shtml',
					data : { paId: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		}
};
$(function() {
	info.loadInfo(1);
});
</script>
</body>
</html>