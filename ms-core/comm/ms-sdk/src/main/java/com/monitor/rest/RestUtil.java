package com.monitor.rest;

import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.monitor.MonitorUtil;
import com.monitor.gray.Gray;
import com.monitor.gray.GrayUser;
import com.monitor.gray.GrayUtil;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameMapUtil;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 服务调度
 * 	V1.0.1：增加将响应的body转换成指定对象的功能
 * @author yuejing
 * @date 2019年3月31日 下午9:17:22
 * @version V1.0.1
 */
@Component
public class RestUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestUtil.class);
	@Autowired
	private LoadBalancerClient loadBalancer;
	@Autowired
	private RestTemplate restTemplate;

	public String getRestUrl(String serviceId, String fallbackUri) {
		if (FrameStringUtil.isEmpty(serviceId)) {
			return URI.create(fallbackUri).toString();
		}
		URI uri = null;
		try {
			ServiceInstance instance = loadBalancer.choose(serviceId);
			uri = instance.getUri();
		} catch (RuntimeException e) {
			uri = URI.create(fallbackUri);
		}
		return uri.toString();
	}

	/**
	 * 发送请求
	 * @param serviceId
	 * @param url
	 * @param params
	 * @return
	 */
	public ResponseFrame request(String serviceId, String url, Map<String, Object> params) {
		return request(serviceId, url, params, null, null);
	}
	/**
	 * 发送请求
	 * @param serviceId
	 * @param url
	 * @param params
	 * @param bodyClazz
	 * @return
	 */
	public ResponseFrame request(String serviceId, String url, Map<String, Object> params, Class<?> bodyClazz) {
		return request(serviceId, url, params, bodyClazz, null);
	}

	/**
	 * 发送请求
	 * @param serviceId
	 * @param url
	 * @param params
	 * @param fallbackUri
	 * @return
	 */
	public ResponseFrame request(String serviceId, String url, Map<String, Object> params, String fallbackUri) {
		return request(serviceId, url, params, null, fallbackUri);
	}

	/**
	 * 发送请求
	 * @param serviceId
	 * @param url
	 * @param params
	 * @param bodyClazz
	 * @param fallbackUri
	 * @return
	 */
	public ResponseFrame request(String serviceId, String url, Map<String, Object> params, Class<?> bodyClazz, String fallbackUri) {
		long begin = System.currentTimeMillis();
		String resResult = null;
		// 处理灰度服务的逻辑
		serviceId = handleGray(serviceId, params);
		// 获取访问服务的前缀地址
		String baseUrl = getRestUrl(serviceId, fallbackUri);
		try {
			HttpHeaders headers = new HttpHeaders();
			// 请勿轻易改变此提交方式，大部分的情况下，提交方式都是表单提交
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			// 封装参数，千万不要替换为Map与HashMap，否则参数无法传递
			MultiValueMap<String, String> paramsMvm = new LinkedMultiValueMap<String, String>();
			Iterator<Entry<String, Object>> entryKeyIterator = params.entrySet().iterator();
			while (entryKeyIterator.hasNext()) {
				Entry<String, Object> e = entryKeyIterator.next();
				String value = null;
				if(e.getValue() != null) {
					value = e.getValue().toString();
				}
				paramsMvm.add(e.getKey(), value);
			}
			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(paramsMvm, headers);
			resResult = restTemplate.postForObject(baseUrl + url, requestEntity, String.class);
			ResponseFrame frame = FrameJsonUtil.toObject(resResult, ResponseFrame.class);
			if (frame.getCode() == ResponseCode.SUCC.getCode() && bodyClazz != null) {
				frame.convertBody(bodyClazz);
			}
			/*if(ResponseCode.SUCC.getCode() != frame.getCode()) {
				String remark = "接口响应异常!";
				saveOptimizeLog(serviceId, url, params, begin, null, null, resResult, remark);
			}*/
			return frame;
		} catch (Exception e) {
			ResponseFrame frame = new ResponseFrame(ResponseCode.SERVER_ERROR);
			Map<String, Object> body = new HashMap<String, Object>();
			body.put("requestParams", params);
			body.put("exceptionInfo", e.getMessage());
			frame.setBody(body);
			e.printStackTrace();
			return frame;
		} finally {
			requestFinally(begin, serviceId, baseUrl, params, resResult);
		}
	}
	
	/**
	 * 处理灰度服务
	 * @param serviceId
	 * @param params
	 * @return
	 */
	private String handleGray(String serviceId, Map<String, Object> params) {
		// 获取灰度用户ID
		String msGuId = FrameMapUtil.getString(params, "msGuId");
		if (FrameStringUtil.isNotEmpty(msGuId)) {
			Gray gray = GrayUtil.get(serviceId);
			if (gray == null) {
				LOGGER.info("灰度用户ID[" + msGuId + "]访问的服务[" + serviceId + "]不存在灰度服务");
			} else {
				// 访问灰度服务
				List<GrayUser> users = gray.getUsers();
				if (users != null && users.size() > 0) {
					for (GrayUser gu : users) {
						if("10".equals(gu.getType())) {
							// 为精确的用户编号
							if (msGuId.equals(gu.getTypeValue())) {
								// 切换为灰度服务
								LOGGER.info("灰度用户ID[" + msGuId + "]访问的服务[" + serviceId + "]切换到灰度服务[" +  gray.getGrayServiceId() + "]");
								serviceId = gray.getGrayServiceId();
							}
						}
					}
				}
			}
		}
		return serviceId;
	}

	private void requestFinally(long begin, String serviceId, String url, Map<String, Object> params, String resResult) {
		long end = System.currentTimeMillis();
		long time = end - begin;
		if(time > 2000) {
			//接口请求>2s，则发送优化信息
			String remark = "请求超时!";
			saveOptimizeLog(serviceId, url, params, begin, end, time, resResult, remark);
		}
	}

	private void saveOptimizeLog(final String serviceId, final String url, final Map<String, Object> params,
			long begin, Long end, Long time, final String resResult, final String remark) {
		if(end == null) {
			end = System.currentTimeMillis();
		}
		if(time == null) {
			time = end - begin;
		}
		final String reqTime = FrameTimeUtil.parseString(new Date(begin));
		final String resTime = FrameTimeUtil.parseString(new Date(end));
		final long useTime = time;
		ThreadPoolTaskExecutor pool = FrameSpringBeanUtil.getBean(ThreadPoolTaskExecutor.class);
		pool.execute(new Runnable() {
			@Override
			public void run() {
				Map<String, Object> paramsMap = new HashMap<String, Object>();
				try {
					paramsMap.put("code", serviceId);
					paramsMap.put("url", url);
					paramsMap.put("params", FrameJsonUtil.toString(params));
					paramsMap.put("reqTime", reqTime);
					paramsMap.put("resTime", resTime);
					paramsMap.put("useTime", useTime);
					paramsMap.put("resResult", resResult);
					paramsMap.put("remark", remark);
					MonitorUtil.post("/api/prjOptimizeLog/save", paramsMap);
				} catch (IOException e) {
				}
			}
		});
	}

	/**
	 * 请求服务端的api
	 * @param url
	 * @param paramsMap
	 * @return
	 * @throws IOException 
	 */
	/*private ResponseFrame post(String url, Map<String, Object> paramsMap) throws IOException {
		String time = String.valueOf(System.currentTimeMillis());
		paramsMap.put("clientId", MonitorCons.clientId);
		paramsMap.put("time", time);
		paramsMap.put("sign", AuthUtil.auth(MonitorCons.clientId, time, MonitorCons.sercret));
		String result = FrameHttpUtil.post(MonitorCons.serverHost + url, paramsMap);
		return FrameJsonUtil.toObject(result, ResponseFrame.class);
	}*/
}