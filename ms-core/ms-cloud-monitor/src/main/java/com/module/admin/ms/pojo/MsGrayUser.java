package com.module.admin.ms.pojo;

import java.io.Serializable;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * ms_gray_user实体
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Alias("msGrayUser")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MsGrayUser extends BaseEntity implements Serializable {
	//灰度服务编号
	private String mgId;
	//类型[10具体用户、20其它]
	private String type;
	//类型值
	private String typeValue;
	//================= Extended attribute
	//名称
	private String name;
	//手机号
	private String phone;
	//备注
	private String remark;
	// 类型名称
	private String typeName;

	public String getMgId() {
		return mgId;
	}
	public void setMgId(String mgId) {
		this.mgId = mgId;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getTypeValue() {
		return typeValue;
	}
	public void setTypeValue(String typeValue) {
		this.typeValue = typeValue;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}