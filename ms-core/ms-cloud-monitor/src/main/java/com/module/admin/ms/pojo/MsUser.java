package com.module.admin.ms.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * ms_user实体
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Alias("msUser")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MsUser extends BaseEntity implements Serializable {
	//编号
	private String muId;
	//名称
	private String name;
	//手机号
	private String phone;
	//备注
	private String remark;
	//创建时间
	private Date createTime;
	//================= Extended attribute
	
	public String getMuId() {
		return muId;
	}
	public void setMuId(String muId) {
		this.muId = muId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}