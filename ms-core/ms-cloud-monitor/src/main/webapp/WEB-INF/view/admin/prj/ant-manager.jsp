<%@page import="com.system.comm.enums.Boolean"%>
<%@page import="com.module.admin.prj.enums.PrjMonitorMonitorStatus"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/inc/sys.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-ANT</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp">
			<jsp:param name="first" value="prj"/>
			<jsp:param name="second" value="prjAntManager"/>
		</jsp:include>
		<div class="c-right">
			<div class="${bodyMainClass}">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">项目 / <b>原型管理</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<c:if test="${param.searchType!=null}"><a href="${webroot}/sysUser/f-view/main.shtml?searchType=${param.searchType}&name=${param.name}" class="btn btn-default btn-sm">返回</a></c:if>
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
				  	<div class="table-tool-panel">
						<div class="row">
							<div class="col-sm-8">
								<div class="btn-group">
								<my:select id="prjId" items="${prjInfos}" headerKey="" headerValue="全部项目" cssCls="form-control input-sm" value="${param.prjId}"/>
								</div>
								<span class="enter-panel">
									<input type="text" style="width: 100px;display: inline;" class="form-control input-sm" id="name" placeholder="名称">
							  		<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
						  		</span>
							</div>
							<div class="col-sm-4 text-right">
							  	<div class="btn-group">
							  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增功能</a>
							  	</div>
							</div>
						</div>
				  	</div>
					<div id="infoPanel" class="table-panel"></div>
					<div id="infoPage" class="table-page-panel"></div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		//获取用户信息
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                        '<th>编码</th>', 
				                        '<th>模块名称</th>',
				                        '<th width="180">创建时间</th>',
				                        '<th width="120">操作</th>',
				                        '</tr></thead><tbody>'].join('');
				infoPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				infoPage.page = page;

			JUtil.ajax({
				url : '${webroot}/prjAnt/f-json/pageQuery.shtml',
				data : { page:infoPage.page, size:infoPage.size,
					prjId:$('#prjId').val(), pid: '0', name: $('#name').val()
				},
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							var _isUseCls = '';
							if(obj.isUse === <%=Boolean.FALSE.getCode()%>) {
								_isUseCls = ' class="text-danger"';
							} else {
								_isUseCls = ' class="text-success"';
							}
							return ['<tr>',
							    	'<td>',obj.paId,'</td>',
							    	'<td>',obj.name,'</td>',
							    	'<td>',obj.createTime,'</td>',
							    	//'<td style="word-wrap:break-word;word-break:break-all;">',obj.method,'</td>',
							    	'<td><a class="glyphicon glyphicon-edit text-success" href="javascript:info.edit(',obj.paId,',',obj.prjId,')" title="修改"></a>',
							    	'&nbsp; <a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(',obj.paId,')" title="删除"></a>',
							    	'&nbsp; &nbsp; <a class="glyphicon text-success" href="javascript:info.func(',obj.paId,',',obj.prjId,')" title="查看功能">查看功能</a>',
							    	'</td>',
								'</tr>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		//编辑
		edit : function(id, prjId) {
			if(id==undefined) {
				prjId = $('#prjId').val();
			}
			dialog({
				title: '编辑',
				url: webroot + '/prjAnt/f-view/edit.shtml?prjId='+prjId+'&paId='+(id?id:''),
				type: 'iframe',
				width: 420,
				height: 230
			});
		},
		//查看功能
		func : function(id, prjId) {
			location = webroot + '/prjAnt/f-view/func.shtml?prjId='+prjId+'&paId='+(id?id:'');
		},
		del : function(id) {
			if(confirm('您确定要删除该记录吗?')) {
				JUtil.ajax({
					url : '${webroot}/prjAnt/f-json/delete.shtml',
					data : { paId: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		}
};
$(function() {
	info.loadInfo(1);
	$('#prjId').change(function() {
		info.loadInfo(1);
	});
});
</script>
</body>
</html>