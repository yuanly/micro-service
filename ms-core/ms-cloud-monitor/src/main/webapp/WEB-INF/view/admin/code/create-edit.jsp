<%@page import="com.module.admin.prj.enums.PrjInfoContainer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑生成源码</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-sm">
		<input type="hidden" id="id" value="${codeCreate.id}">
		<input type="hidden" id="code" value="${param.code}">
  		<div class="form-group">
			<label for="packagePath" class="col-sm-4">包路径 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="packagePath" placeholder="功能包路径" value="${codeCreate.packagePath}"></div>
		</div>
  		<div class="form-group">
			<label for="dsCode" class="col-sm-4">数据源 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:select id="dsCode" headerKey="" headerValue="请选择数据源" items="${dsList}" value="${codeCreate.dsCode}" cssCls="form-control"/></div>
		</div>
  		<div class="form-group">
			<label for="dbName" class="col-sm-4">数据库名 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="dbName" placeholder="数据库名或sid" value="${codeCreate.dbName}"></div>
		</div>
  		<div class="form-group">
			<label for="tables" class="col-sm-4">生成表名</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="tables" placeholder="多个表用,分隔" value="${codeCreate.tables}"></div>
		</div>
		<div class="text-right"><small><a href="javascript:;" onclick="info.loadTables(this)">加载所有表</a></small></div>
  		<div class="form-group" id="tablePanel">
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	var info = {
			selectTable: function(_this) {
				var _orgTables = $('#tables').val();
				_orgTables = _orgTables.split(",");
				var tables = [];
				var check = $(_this).prop('checked');
				var value = $(_this).val();
				$.each(_orgTables, function(i, obj) {
					//console.log(obj + '||' + value);
					if (obj != value && JUtil.isNotEmpty(obj)) {
						tables.push(obj);
					}
				});
				if (check) {
					// 添加
					tables.push(value);
				}
				$('#tables').val(tables.join(','));
				/* $('input[name="tables"]').each(function(i, obj) {
					$.each(_orgTables, function(j, ot) {
						if(ot == $(obj).val()) {
							$(obj).attr('checked', 'checked');
						}
					});
				}); */
			},
			loadTables: function(_this) {
				var _saveMsg = $('#saveMsg').empty();
				_saveMsg.attr('class', 'label label-danger');
				var _dsCode = $('#dsCode');
				if(JUtil.isEmpty(_dsCode.val())) {
					_saveMsg.append('请输选择数据源');
					_dsCode.focus();
					return;
				}
				var _dbName = $('#dbName');
				if(JUtil.isEmpty(_dbName.val())) {
					_saveMsg.append('请输入数据库或sid的名称');
					_dbName.focus();
					return;
				}
				var org = $(_this).html();
				$(_this).html('加载中...');
				var _tablePanel = $('#tablePanel').empty();
				JUtil.ajax({
					url : '${webroot}/codeCreate/f-json/findTables.shtml',
					data : { dsCode: _dsCode.val(), prjId: '${param.prjId}',dbName: _dbName.val() },
					success : function(json) {
						_tablePanel.empty();
						if (json.code === 0) {
							var _conts = ['<hr/><div>'];
							$.each(json.body, function(i, obj) {
								_conts.push('<div class="col-sm-4"><label><input type="checkbox" onclick="info.selectTable(this)" name="tables" value="',obj,'"> <small>',obj,'</small></label></div>');
							});
							_conts.push('</div>');
							_tablePanel.append(_conts.join(''));

							/* var _orgTables = '${codeCreate.tables}';
							_orgTables = _orgTables.split(",");
							$('input[name="tables"]').each(function(i, obj) {
								$.each(_orgTables, function(j, ot) {
									if(ot == $(obj).val()) {
										$(obj).attr('checked', 'checked');
									}
								});
							}); */
						}
						else if (json.code === -1)
							_saveMsg.append(JUtil.msg.ajaxErr);
						else
							_saveMsg.append(json.message);
						$(_this).html(org);
					}
				});
			}
	};
	$(function() {
		/* if(JUtil.isNotEmpty($('#id').val())) {
			info.loadTables();
		} */
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			_saveMsg.attr('class', 'label label-danger');

			var _packagePath = $('#packagePath');
			if(JUtil.isEmpty(_packagePath.val())) {
				_saveMsg.append('请输入功能包路径');
				_packagePath.focus();
				return;
			}
			var _dsCode = $('#dsCode');
			if(JUtil.isEmpty(_dsCode.val())) {
				_saveMsg.append('请输选择数据源');
				_dsCode.focus();
				return;
			}
			var _dbName = $('#dbName');
			if(JUtil.isEmpty(_dbName.val())) {
				_saveMsg.append('请输入数据库或sid的名称');
				_dbName.focus();
				return;
			}

			/* var _tables = [];
			$('input[name="tables"]:checked').each(function(i, obj) {
				_tables.push($(obj).val());
			});
			if(_tables.length == 0) {
				_saveMsg.append('请选择要生成的表');
				return;
			} */
			var tables = $('#tables');
			if(JUtil.isEmpty(tables.val())) {
				_saveMsg.append('请输入或选择要生成的表名');
				tables.focus();
				return;
			}
			
			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/codeCreate/f-json/save.shtml',
				data : {
					id: $('#id').val(),
					code: $('#code').val(),
					packagePath: _packagePath.val(),
					dsCode: _dsCode.val(),
					dbName: _dbName.val(),
					tables: tables.val()
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.create.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>