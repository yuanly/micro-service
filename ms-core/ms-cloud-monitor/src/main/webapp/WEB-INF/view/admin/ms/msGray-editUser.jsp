<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑灰度服务记录</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
		<input type="hidden" id="mgId" value="${param.mgId}">
		<div class="form-group">
			<label for="type" class="col-sm-4">类型 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:radio id="type" name="type" dictcode="ms_gray_user_type" value="" defvalue="10" exp="onclick=\"info.changeType($(this).val())\"" /></div>
		</div>
  		<div class="form-group" id="msUserPanel">
			<label for="msUserName" class="col-sm-4 va-top">用户信息 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<div class="table-tool-panel">
					<input type="text" class="form-control input-sm w-120" id="name" placeholder="名称" value="">
					<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
				</div>
				<div id="infoPanel" class="table-panel" style="min-height: 150px"></div>
				<div id="infoPage" class="table-page-panel"></div>
			</div>
		</div>
		<div id="typeValuePanel">
	  		<div class="form-group">
				<label for="typeValueOther" class="col-sm-4">类型值 <span class="text-danger">*</span></label>
				<div class="col-sm-8"><input type="text" class="form-control" id="typeValueOther" placeholder="类型的值" value=""></div>
			</div>
	  		<div class="form-group">
				<label for="" class="col-sm-4">&nbsp;</label>
				<div class="col-sm-8"><button type="button" class="btn btn-sm btn-success" onclick="info.addOther()">添加</button></div>
			</div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" onclick="parent.dialog.close()" class="btn btn-default">关闭</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
	<script type="text/javascript">

	var infoPage = undefined;
	var info = {
			init: function() {
				info.changeType('10');
			},
			changeType: function(value) {
				if (value == '10') {
					$('#msUserPanel').css('display', 'block');
					$('#typeValuePanel').css('display', 'none');
					info.loadInfo(1);
				} else {
					$('#msUserPanel').css('display', 'none');
					$('#typeValuePanel').css('display', 'block');
				}
			},
			//获取用户信息
			loadInfo : function(page) {
				if(!infoPage) {
					infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
					infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="default">',
					                         '<th>编码</th>',
					                         '<th>名称</th>',
					                         '<th>电话</th>',
					                         '<th width="80">操作</th>',
					                         '</tr></thead><tbody>'].join('');
					infoPage.endString = '</tbody></table>';
				}
				if(page != undefined)
					infoPage.page = page;

				JUtil.ajax({
					url : '${webroot}/msUser/f-json/pageQuery.shtml',
					data : { page:infoPage.page, size:infoPage.size, name: $('#name').val() },
					beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
					error : function(json){ infoPage.error('加载信息出错了!'); },
					success : function(json){
						if(json.code === 0) {
							function getResult(obj) {
								return ['<tr>',
								    	'<td>',obj.muId,'</td>',
								    	'<td>',obj.name,'</td>',
								    	'<td>',obj.phone,'</td>',
								    	'<td><a class="text-success" href="javascript:info.add(\'',obj.muId,'\')" title="添加到灰度服务">添加</a>',
								    	'</td>',
									'</tr>'].join('');
							}
							infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
						}
						else alert(JUtil.msg.ajaxErr);
					}
				});
			},
			addOther: function() {
				var saveMsg = $('#saveMsg').empty();
				var typeValueOther = $('#typeValueOther');
				if (JUtil.isEmpty(typeValueOther.val())) {
					saveMsg.append('请输入类型值');
					typeValueOther.focus();
					return;
				}
				info.add(typeValueOther.val());
			},
			add: function(typeValue) {
				var saveMsg = $('#saveMsg').empty();
				
				JUtil.ajax({
					eventId: 'saveBtn',
					url: '${webroot}/msGray/f-json/saveUser.shtml',
					data: {
						mgId: $('#mgId').val(),
						type: $('input[name="type"]:checked').val(),
						typeValue: typeValue
					},
					success : function(json) {
						if (json.code === 0) {
							saveMsg.attr('class', 'label label-success').append('添加成功');
							setTimeout(function() {
								parent.right.loadInfo();
							}, 800);
						}
						else if (json.code === -1)
							saveMsg.append(JUtil.msg.ajaxErr);
						else
							saveMsg.append(json.message);
					}
				});
			}
	};
	$(function() {
		info.init();
	});
	</script>
</body>
</html>