<%@page import="com.system.comm.enums.Boolean"%>
<%@page import="com.module.admin.prj.enums.PrjMonitorMonitorStatus"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/inc/sys.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-系统优化</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp">
			<jsp:param name="first" value="prj"/>
			<jsp:param name="second" value="prjOptimizeLogManager"/>
		</jsp:include>
		<div class="c-right">
			<div class="${bodyMainClass}">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">项目 / <b>系统优化</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
								<c:if test="${param.searchType!=null}"><a href="${webroot}/sysUser/f-view/main.shtml?searchType=${param.searchType}&name=${param.name}" class="btn btn-default btn-sm">返回</a></c:if>
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
				  	<div class="table-tool-panel">
						<div class="row">
							<div class="col-sm-8">
								<div class="btn-group">
								<my:select id="prjId" items="${prjInfos}" headerKey="" headerValue="全部项目" cssCls="form-control input-sm" value="${param.prjId}"/>
								</div>
								<span class="enter-panel">
									<!-- <input type="text" style="width: 100px;display: inline;" class="form-control input-sm" id="name" placeholder="名称"> -->
							  		<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
						  		</span>&nbsp;
						  		<small class="text-muted">默认响应时间超过2s则会进入优化范围</small>
							</div>
							<div class="col-sm-4 text-right">
								<div class="btn-group">
							  		<a href="javascript:;" class="btn btn-danger btn-sm" onclick="info.deleteAll()">清空记录</a>
							  	</div>
							  	<div class="btn-group">
							  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.setRule()">设置规则</a>
							  	</div>
							</div>
						</div>
				  	</div>
					<div id="infoPanel" class="table-panel"></div>
					<div id="infoPage" class="table-page-panel"></div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		//获取用户信息
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                        '<th>编码</th>', 
				                        '<th>所属项目</th>',
				                        '<th>接口地址</th>',
				                        '<th width="160">请求时间</th>',
				                        '<th width="160">响应时间</th>',
				                        '<th width="80">用时</th>',
				                        '<th width="120">响应数据大小</th>',
				                        '<th>备注</th>',
				                        '<th width="80">操作</th>',
				                        '</tr></thead><tbody>'].join('');
				infoPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				infoPage.page = page;

			JUtil.ajax({
				url : '${webroot}/prjOptimizeLog/f-json/pageQuery.shtml',
				data : { page:infoPage.page, size:infoPage.size,
					prjId:$('#prjId').val()
				},
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							return ['<tr>',
							    	'<td>',obj.id,'</td>',
							    	'<td>',obj.prjName,'</td>',
							    	'<td>',obj.url,'</td>',
							    	'<td>',obj.reqTime,'</td>',
							    	'<td>',obj.resTime,'</td>',
							    	'<td>',obj.useTime,'ms</td>',
							    	'<td>',obj.resSize,'kb</td>',
							    	'<td>',obj.remark,'</td>',
							    	//'<td style="word-wrap:break-word;word-break:break-all;">',obj.method,'</td>',
							    	'<td><a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(',obj.id,')" title="删除"></a>',
							    	'&nbsp; &nbsp;<a class="text-success" href="javascript:info.look(',obj.id,')" title="查看详情">详情</a>',
							    	'</td>',
								'</tr>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		//查看详情
		look : function(id) {
			dialog({
				title: '查看详情',
				url: webroot + '/prjOptimizeLog/f-view/look.shtml?id='+(id?id:''),
				type: 'iframe',
				width: 520,
				height: 400
			});
		},
		//清空记录
		del : function(id) {
			if(confirm('您确定要删除该记录吗?')) {
				JUtil.ajax({
					url : '${webroot}/prjOptimizeLog/f-json/delete.shtml',
					data : { id: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		},
		deleteAll : function(id) {
			if(confirm('您确定要删除所有记录吗?')) {
				JUtil.ajax({
					url : '${webroot}/prjOptimizeLog/f-json/deleteAll.shtml',
					data : { },
					success : function(json) {
						if (json.code === 0) {
							message('删除所有记录成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		},
		//设置规则
		setRule : function() {
			location = webroot + '/prjOptimize/f-view/manager.shtml?prjId=' + $('#prjId').val();
		}
};
$(function() {
	info.loadInfo(1);
	$('#prjId').change(function() {
		info.loadInfo(1);
	});
});
</script>
</body>
</html>