package com.frame.test.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 测试
 * @author yuejing
 * @date 2017年2月20日 上午9:08:03
 */
@Controller
public class TestController {

    private final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    /*@RequestMapping(value = "/add" ,method = RequestMethod.GET)
    public Integer add(@RequestParam Integer a, @RequestParam Integer b) {
        ServiceInstance instance = client.getLocalServiceInstance();
        Integer r = a + b;
        logger.info("/add, host:" + instance.getHost() + ", service_id:" + instance.getServiceId() + ", result:" + r);
        return r;
    }*/
    
    @RequestMapping(value = "/test/index")
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("message", "您好");
        LOGGER.info("您好");
        return "test/index";
    }
}
