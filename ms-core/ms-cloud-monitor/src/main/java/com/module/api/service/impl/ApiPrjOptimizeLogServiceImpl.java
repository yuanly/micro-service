package com.module.api.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.prj.pojo.PrjInfo;
import com.module.admin.prj.pojo.PrjOptimize;
import com.module.admin.prj.pojo.PrjOptimizeLog;
import com.module.admin.prj.service.PrjInfoService;
import com.module.admin.prj.service.PrjOptimizeLogService;
import com.module.admin.prj.service.PrjOptimizeService;
import com.module.api.service.ApiPrjOptimizeLogService;
import com.system.handle.model.ResponseFrame;

@Component
public class ApiPrjOptimizeLogServiceImpl implements ApiPrjOptimizeLogService {

	@Autowired
	private PrjOptimizeLogService prjOptimizeLogService;
	@Autowired
	private PrjOptimizeService prjOptimizeService;
	@Autowired
	private PrjInfoService prjInfoService;
	
	@Override
	public ResponseFrame save(String code, String url, String params,
			Date reqTime, Date resTime, Integer useTime, String resResult, String remark) {
		ResponseFrame frame = new ResponseFrame();
		PrjInfo prj = prjInfoService.getCode(code);
		if(prj == null) {
			frame.setCode(-2);
			frame.setMessage("项目不存在");
			return frame;
		}
		if(useTime == null) {
			useTime = (int) (resTime.getTime() - reqTime.getTime());
		}
		PrjOptimize opt = prjOptimizeService.getByPrjIdUrl(prj.getPrjId(), url);
		if(opt != null && useTime.intValue() < opt.getWarnTime().intValue()) {
			frame.setCode(-2);
			frame.setMessage("实际使用时间小于优化规则的时间");
			return frame;
		}
		PrjOptimizeLog log = new PrjOptimizeLog();
		log.setPrjId(prj.getPrjId());
		log.setUrl(url);
		log.setParams(params);
		log.setReqTime(reqTime);
		log.setResTime(resTime);
		log.setUseTime(useTime);
		log.setResResult(resResult);
		log.setResSize((double)resResult.length()/1000);
		log.setRemark(remark);
		prjOptimizeLogService.save(log);
		frame.setSucc();
		return frame;
	}
}