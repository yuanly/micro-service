package com.module.admin.sys.service;

import org.springframework.stereotype.Component;

import com.module.admin.sys.pojo.SysUserAtt;
import com.system.handle.model.ResponseFrame;

/**
 * sys_user_att的Service
 * @author autoCode
 * @date 2018-06-12 09:59:57
 * @version V1.0.0
 */
@Component
public interface SysUserAttService {
	
	/**
	 * 保存或修改
	 * @param sysUserAtt
	 * @return
	 */
	public ResponseFrame save(SysUserAtt sysUserAtt);
	
	/**
	 * 根据userId获取对象
	 * @param userId
	 * @return
	 */
	public SysUserAtt get(Integer userId, String type, String typeNo);

	/**
	 * 分页获取对象
	 * @param sysUserAtt
	 * @return
	 */
	public ResponseFrame pageQuery(SysUserAtt sysUserAtt);
	
	/**
	 * 根据userId删除对象
	 * @param userId
	 * @param type 
	 * @param typeNo 
	 * @return
	 */
	public ResponseFrame delete(Integer userId, String type, String typeNo);
}