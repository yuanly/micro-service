package com.module.admin.ms.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.module.admin.ms.pojo.MsUser;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseFrame;

/**
 * ms_user的Service
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Component
public interface MsUserService {
	
	/**
	 * 保存或修改
	 * @param msUser
	 * @return
	 */
	public ResponseFrame saveOrUpdate(MsUser msUser);
	
	/**
	 * 根据muId获取对象
	 * @param muId
	 * @return
	 */
	public MsUser get(String muId);

	/**
	 * 分页获取对象
	 * @param msUser
	 * @return
	 */
	public ResponseFrame pageQuery(MsUser msUser);
	
	/**
	 * 根据muId删除对象
	 * @param muId
	 * @return
	 */
	public ResponseFrame delete(String muId);

	public List<KvEntity> findSearch(String searchString);
}