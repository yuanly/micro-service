package com.ms.biz.admin.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import com.monitor.MonitorCons;
import com.monitor.MonitorUtil;

@Configuration
public class MonitorConfig {

	private final Logger LOGGER = LoggerFactory.getLogger(MonitorConfig.class);

	@Autowired
	private Environment env;
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public MonitorUtil monitorUtil() throws Exception {
		LOGGER.info("初始化Monitor的信息");
		MonitorCons.clientId = env.getProperty("client.monitor.id");
		MonitorCons.sercret = env.getProperty("client.monitor.token");
		MonitorCons.serverId = env.getProperty("client.monitor.server.id");
		MonitorCons.serverHost = env.getProperty("client.monitor.server.host");
		MonitorUtil monitorUtil = new MonitorUtil(env);
		monitorUtil.init();
		return monitorUtil;
	}
}