package com.monitor;

/**
 * 监控的API信息
 * @author yuejing
 * @date 2017年6月13日 下午3:35:09
 */
public class MonitorCons {

	/**
	 * Monitor的客户端ID
	 */
	public static String clientId;
	/**
	 * Monitor的密钥
	 */
	public static String sercret;
	/**
	 * Monitor的服务ID
	 */
	public static String serverId;
	/**
	 * Monitor的服务地址前缀[做为备用地址前缀]
	 */
	public static String serverHost;
	
}
