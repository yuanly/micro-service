package com.monitor.gray;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * ms_gray实体
 * @author yuejing
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Gray extends BaseEntity implements Serializable {
	//编号
	private String mgId;
	//正常服务ID
	private String normalServiceId;
	//灰度服务ID
	private String grayServiceId;
	//================= Extended attribute
	// 灰度用户列表
	private List<GrayUser> users;
	
	public String getMgId() {
		return mgId;
	}
	public void setMgId(String mgId) {
		this.mgId = mgId;
	}
	
	public String getNormalServiceId() {
		return normalServiceId;
	}
	public void setNormalServiceId(String normalServiceId) {
		this.normalServiceId = normalServiceId;
	}
	
	public String getGrayServiceId() {
		return grayServiceId;
	}
	public void setGrayServiceId(String grayServiceId) {
		this.grayServiceId = grayServiceId;
	}
	
	public List<GrayUser> getUsers() {
		return users;
	}
	public void setUsers(List<GrayUser> users) {
		this.users = users;
	}
}