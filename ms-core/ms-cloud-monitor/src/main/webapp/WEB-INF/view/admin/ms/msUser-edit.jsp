<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑微服务用户</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
  		<div class="form-group">
			<label for="muId" class="col-sm-4">编码 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="muId" placeholder="用户编码" value="${msUser.muId}"></div>
		</div>
  		<div class="form-group">
			<label for="name" class="col-sm-4">名称 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="name" placeholder="名称" value="${msUser.name}"></div>
		</div>
  		<div class="form-group">
			<label for="phone" class="col-sm-4">手机号</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="phone" placeholder="手机号" value="${msUser.phone}"></div>
		</div>
  		<div class="form-group">
			<label for="remark" class="col-sm-4">备注</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="remark" placeholder="备注" value="${msUser.remark}"></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var saveMsg = $('#saveMsg').empty();
			var muId = $('#muId');
			if(JUtil.isEmpty(muId.val())) {
				saveMsg.append('请输入用户编码');
				muId.focus();
				return;
			}
			
			var name = $('#name');
			if(JUtil.isEmpty(name.val())) {
				saveMsg.append('请输入名称');
				name.focus();
				return;
			}

			JUtil.ajax({
				eventId: 'saveBtn',
				url: '${webroot}/msUser/f-json/save.shtml',
				data: {
					muId: muId.val(),
					name: name.val(),
					phone: $('#phone').val(),
					remark: $('#remark').val()
				},
				success : function(json) {
					if (json.code === 0) {
						saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						saveMsg.append(JUtil.msg.ajaxErr);
					else
						saveMsg.append(json.message);
				}
			});
		});
	});
	</script>
</body>
</html>