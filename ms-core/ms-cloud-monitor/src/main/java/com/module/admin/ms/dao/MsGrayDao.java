package com.module.admin.ms.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.module.admin.ms.pojo.MsGray;

/**
 * ms_gray的Dao
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
public interface MsGrayDao {

	public abstract void save(MsGray msGray);

	public abstract void update(MsGray msGray);

	public abstract void delete(@Param("mgId")String mgId);

	public abstract MsGray get(@Param("mgId")String mgId);

	public abstract List<MsGray> findMsGray(MsGray msGray);
	
	public abstract int findMsGrayCount(MsGray msGray);

	public abstract List<MsGray> findUse();
}