package com.frame.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frame.test.pojo.Test;
import com.frame.test.service.TestService;

/**
 * 测试
 * @author yuejing
 * @date 2017年2月20日 上午9:08:03
 */
@Controller
public class TestController {

    //private final MsLog MSLOG = MsLog.getMsLog(TestController.class);
    
    @Autowired
    private TestService testService;

    /*@RequestMapping(value = "/add" ,method = RequestMethod.GET)
    public Integer add(@RequestParam Integer a, @RequestParam Integer b) {
        ServiceInstance instance = client.getLocalServiceInstance();
        Integer r = a + b;
        logger.info("/add, host:" + instance.getHost() + ", service_id:" + instance.getServiceId() + ", result:" + r);
        return r;
    }*/
    
    //http://127.0.0.1:7950/test/index
    @RequestMapping(value = "/test/index")
    public String index(ModelMap modelMap) {
    	String value = testService.get("您好", "");
        modelMap.addAttribute("message", value);
		//MSLOG.info(modelMap);
        return "index";
    }
    

    @RequestMapping(value = "/test/findAll")
    @ResponseBody
    public List<Test> findAll() {
    	List<Test> list = testService.findAll();
		//MSLOG.info(modelMap);
        return list;
    }

    @RequestMapping(value = "/test/{id}")
    @ResponseBody
    public String get(ModelMap modelMap,
    		@PathVariable("id")String id, String msGuId) {
    	String value = testService.get(id, msGuId);
		//MSLOG.info(modelMap);
        return value;
    }
}