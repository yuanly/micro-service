package com.system.handle.model;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.config.SystemConfig;
import com.system.comm.utils.FrameJsonUtil;

/**
 * 响应框架实体
 * @author yuejing
 * @date 2016年1月29日 下午9:29:33
 * @version V1.0.0
 */
@JsonInclude(Include.NON_NULL)
public class ResponseFrame implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer code = 0;
    private String message = "success";
    private Long time = System.currentTimeMillis();
    private String version = SystemConfig.getVersion();
    private Object body = new HashMap<String, Object>();

    /**
     * code默认为成功
     */
    public ResponseFrame() {
        //默认为成功
    	this.code = ResponseCode.SUCC.getCode();
    }

    public ResponseFrame(Integer code, String message) {
    	this.code = code;
        this.message = message;
    }

    public ResponseFrame(Integer code, Object body) {
    	this.code = code;
        this.body = body;
    }

    public ResponseFrame(ResponseCode errorCode) {
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
    }

    public ResponseFrame(ResponseCode errorCode, Map<String, Object> body) {
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
        this.body = body;

    }

    public Integer getCode() {
        return code;
    }
    public void setCode(Integer code) {
        this.code = code;
    }
    public void setSucc() {
        this.code = ResponseCode.SUCC.getCode();
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @SuppressWarnings("unchecked")
	public <T>T getBody() {
        return (T) body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
    /**
     * 转换body对象的类型
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
	public <T>T convertBody(Class<?> clazz) {
    	String result = FrameJsonUtil.toString(this.body);
    	if (this.body instanceof List) {
            this.body = FrameJsonUtil.toList(result, clazz);
    	} else {
    		this.body = FrameJsonUtil.toObject(result, clazz);
    	}
        return (T) this.body;
    }
}