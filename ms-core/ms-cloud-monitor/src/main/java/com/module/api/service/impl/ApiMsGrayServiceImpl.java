package com.module.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.ms.pojo.MsGray;
import com.module.admin.ms.pojo.MsGrayUser;
import com.module.admin.ms.service.MsGrayService;
import com.module.admin.ms.service.MsGrayUserService;
import com.module.api.service.ApiMsGrayService;
import com.system.handle.model.ResponseFrame;

@Component
public class ApiMsGrayServiceImpl implements ApiMsGrayService {

	@Autowired
	private MsGrayService msGrayService;
	@Autowired
	private MsGrayUserService msGrayUserService;

	@Override
	public ResponseFrame findUse() {
		ResponseFrame frame = new ResponseFrame();
		List<MsGray> list = msGrayService.findUse();
		for (MsGray mg : list) {
			// 获取灰度用户
			List<MsGrayUser> users = msGrayUserService.findByMgId(mg.getMgId());
			if (users.size() > 0) {
				mg.setUsers(users);
			}
		}
		frame.setBody(list);
		frame.setSucc();
		return frame;
	}
	

}