<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/inc/sys.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-灰度服务</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp">
			<jsp:param name="first" value="ms"/>
			<jsp:param name="second" value="msGrayManager"/>
		</jsp:include>
		<div class="c-right">
			<div class="${bodyMainClass}">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">微服务 / <b>灰度服务</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<c:if test="${param.searchType!=null}"><a href="${webroot}/sysUser/f-view/main.shtml?searchType=${param.searchType}&name=${param.name}" class="btn btn-default btn-sm">返回</a></c:if>
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
				  		<div class="col-sm-7">
				  			<div class="table-tool-panel">
								<div class="row">
									<div class="col-sm-6 enter-panel">
										<input type="text" class="form-control input-sm w-120" id="normalServiceId" placeholder="正常服务ID" value="${param.name}">
									  	<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
									</div>
									<div class="col-sm-6 text-right">
										
									  	<div class="btn-group">
									  		<a href="javascript:;" class="btn btn-default btn-sm" onclick="alert('请求接口时，传入灰度用户的信息key：msGuId。灰度用户的value为具体的灰度用户ID值。')">应用中使用</a>
									  	</div>
									  	<div class="btn-group">
									  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增灰度服务</a>
									  	</div>
									</div>
								</div>
						  	</div>
							<div id="infoPanel" class="table-panel"></div>
							<div id="infoPage" class="table-page-panel"></div>
				  		</div>
				  		<div class="col-sm-5">
				  			<div id="rightInfoMsg" class="padding-top-10">
				  				<label class="label label-warning">暂无关联的用户信息~</label>
				  			</div>
				  			<div id="rightInfo" style="display: none;">
				  			<div class="table-tool-panel">
					  			<div class="row">
									<div class="col-sm-6 enter-panel">
										<input type="text" class="form-control input-sm w-120" id="typeValue" placeholder="类型的值(用户的名称等)" value="">
									  	<button type="button" class="btn btn-sm btn-default enter-fn" onclick="right.loadInfo(1)">查询</button>
									</div>
									<div class="col-sm-6 text-right">
										<div class="btn-group">
									  	</div>
									  	<div class="btn-group">
									  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="right.edit()">+ 记录</a>
									  	</div>
									</div>
								</div>
						  	</div>
							<div id="rightPanel" class="table-panel"></div>
							<div id="rightPage" class="table-page-panel"></div>
							</div>
				  		</div>
				  	</div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		//获取用户信息
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                         '<th>正常服务ID</th>',
				                         '<th>灰度服务ID</th>',
				                         '<th>是否使用</th>',
				                         '<th width="110">操作</th>',
				                         '</tr></thead><tbody>'].join('');
				infoPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				infoPage.page = page;

			JUtil.ajax({
				url : '${webroot}/msGray/f-json/pageQuery.shtml',
				data : { page:infoPage.page, size:infoPage.size, normalServiceId: $('#normalServiceId').val() },
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							var trClass = '';
							if(right.id === undefined) {
								//为第一次进来，默认加载发布的项
								right.id = obj.mgId;
								right.loadInfo(1);
								trClass = 'success';
							}
							return ['<tr class="',trClass,'">',
							    	'<td>',obj.normalServiceId,'</td>',
							    	'<td>',obj.grayServiceId,'</td>',
							    	'<td>',obj.isUseName,'</td>',
							    	'<td><a class="glyphicon glyphicon-edit text-success" href="javascript:info.edit(\'',obj.mgId,'\')" title="修改"></a>',
							    	'&nbsp; <a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(\'',obj.mgId,'\')" title="删除"></a>',
							    	'&nbsp; <a class="text-success" href="javascript:;" onclick="info.relUser(this,\'',obj.mgId,'\')" title="去关联微服务的用户">关联用户</a>',
							    	'</td>',
								'</tr>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		//编辑
		edit : function(id) {
			dialog({
				title: '编辑灰度服务',
				url: webroot + '/msGray/f-view/edit.shtml?mgId='+(id?id:''),
				type: 'iframe',
				width: 420,
				height: 300
			});
		},
		relUser: function(_this, id) {
			var tr = $(_this).parent().parent();
			tr.parent().find('tr').each(function(i, obj) {
				$(obj).removeClass('success');
			});
			tr.addClass('success');
			right.id = id;
			right.loadInfo(1);
		},
		del : function(id) {
			if(confirm('您确定要删除该灰度服务吗?')) {
				JUtil.ajax({
					url : '${webroot}/msGray/f-json/delete.shtml',
					data : { mgId: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo(1);
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		}
};


var rightPage = undefined;
var right = {
		//获取用户信息
		loadInfo : function(page) {
			if(right.id != undefined) {
				$('#rightInfo').css('display', 'block');
				$('#rightInfoMsg').css('display', 'none');
			}
			if(!rightPage) {
				rightPage = new Page('rightPage', right.loadInfo, 'rightPanel', 'rightPage');
				rightPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                         '<th>类型</th>',
				                         '<th>值</th>',
				                         '<th width="60">操作</th>',
				                         '</tr></thead><tbody>'].join('');
				rightPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				rightPage.page = page;

			JUtil.ajax({
				url : '${webroot}/msGray/f-json/pageQueryUser.shtml',
				data : { page:rightPage.page, size:rightPage.size, mgId: right.id, typeValue: $('#typeValue').val() },
				beforeSend: function(){ rightPage.beforeSend('加载信息中...'); },
				error : function(json){ rightPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							var typeValue = '';
							if (obj.type == 10) {
								typeValue = obj.name + ' [' + obj.typeValue + ']';
							} else {
								typeValue = obj.typeValue;
							}
							return ['<tr>',
							    	'<td>',obj.typeName,'</td>',
							    	'<td>',typeValue,'</td>',
							    	'<td><a class="glyphicon glyphicon-remove text-success" href="javascript:right.del(\'',obj.type,'\',\'',obj.typeValue,'\')" title="删除"></a>',
							    	'</td>',
								'</tr>'].join('');
						}
						rightPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
						JUtil.sys.initOptMore();
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		edit : function() {
			dialog({
				title: '新增灰度服务记录',
				url: webroot + '/msGray/f-view/editUser.shtml?mgId='+right.id,
				type: 'iframe',
				width: 520,
				height: 420
			});
		},
		del : function(type, typeValue) {
			if(confirm('您确定要删除该记录吗?')) {
				JUtil.ajax({
					url : '${webroot}/msGray/f-json/deleteUser.shtml',
					data : { mgId: right.id, type: type, typeValue: typeValue },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							right.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		}
};
$(function() {
	info.loadInfo(1);
});
</script>
</body>
</html>