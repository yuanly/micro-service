<%@page import="com.system.comm.enums.Boolean"%>
<%@page import="com.module.admin.prj.enums.PrjMonitorMonitorStatus"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-API测试</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp">
			<jsp:param name="first" value="prj"/>
			<jsp:param name="second" value="prjInfoManager"/>
		</jsp:include>
		<div class="c-right">
			<div class="${bodyMainClass}">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">项目 / <b>API测试</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<a href="javascript:history.go(-1)" class="btn btn-default btn-sm">返回</a>
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
				  		<div class="col-sm-5">
						  	<div class="table-tool-panel">
								<div class="row">
									<div class="col-sm-9">
										<div class="btn-group">
										<my:select id="prjId" items="${prjInfos}" headerKey="" headerValue="全部项目" cssCls="form-control input-sm" value="${param.prjId}" exp="style=\"width:120px;\""/>
										</div>
										<span class="enter-panel">
											<input type="text" style="width: 100px;display: inline;" class="form-control input-sm" id="name" placeholder="名称">
									  		<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
								  		</span>
									</div>
									<div class="col-sm-3 text-right">
									  	<div class="btn-group">
									  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增测试</a>
									  	</div>
									</div>
								</div>
						  	</div>
							<div id="infoPanel" class="table-panel"></div>
							<div id="infoPage" class="table-page-panel"></div>
						</div>
						<div class="col-sm-7">
				  			<div id="apiDtlInfoMsg" class="text-center" style="margin-top: 10px;"><label class="label label-warning">暂无需要设置的API的信息~</label></div>
				  			<div id="apiDtlInfo" style="display: none;">
				  			<div class="table-tool-panel">
					  			<div class="row">
									<div class="col-sm-6 text-muted" id="apiDtlInfoTitle">
									</div>
									<div class="col-sm-6 text-right">
										<div class="btn-group">
											<select id="refreshInterval" onchange="apiDtl.refreshChange()" class="form-control input-sm" style="width: 90px;">
												<option value="">刷新:关闭</option>
												<option value="3">3s刷新一次</option>
												<option value="5">5s刷新一次</option>
												<option value="10">10s刷新一次</option>
											</select>
									  	</div>
									  	<div class="btn-group">
									  		<a href="javascript:;" class="btn btn-danger btn-sm" onclick="apiDtl.test()">全部测试</a>
									  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="apiDtl.edit()">增加路径</a>
									  	</div>
									</div>
								</div>
						  	</div>
							<div id="apiDtlPanel" class="table-panel"></div>
							<div id="apiDtlPage" class="table-page-panel"></div>
							</div>
				  		</div>
					</div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		//获取用户信息
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                         '<th>编号</th>',
				                         '<th>名称</th>',
				                         '<th width="150">操作</th>',
				                         '</tr></thead><tbody>'].join('');
				infoPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				infoPage.page = page;

			JUtil.ajax({
				url : '${webroot}/prjApiTest/f-json/pageQuery.shtml',
				data : { page:infoPage.page, size:infoPage.size,
					prjId:$('#prjId').val(), name: $('#name').val()
				},
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							if(apiDtl.patId === undefined) {
								//为第一次进来，默认加载发布的项
								info.setApi(obj.id, obj.name);
							}
							return ['<tr>',
							    	'<td>',obj.id,'</td>',
							    	'<td>',obj.name,'</td>',
							    	'<td><a class="glyphicon glyphicon-edit text-success" href="javascript:info.edit(\'',obj.id,'\')" title="修改"></a>',
							    	'&nbsp; <a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(\'',obj.id,'\')" title="删除"></a>',
							    	'&nbsp; <a class="glyphicon text-success" href="javascript:info.setApi(\'',obj.id,'\',\'',obj.name,'\')" title="设置要测试的API列表">设置API</a>',
							    	'&nbsp; &nbsp; <a class="glyphicon text-success" href="javascript:info.lookResult(\'',obj.id,'\')" title="查看测试结果">看结果</a>',
							    	'</td>',
								'</tr>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		//编辑
		edit : function(id) {
			dialog({
				title: '编辑',
				url: webroot + '/prjApiTest/f-view/edit.shtml?prjId='+$('#prjId').val()+'&id='+(id?id:''),
				type: 'iframe',
				width: 420,
				height: 240
			});
		},
		lookResult : function(id) {
			JUtil.ajax({
				url : '${webroot}/prjApiTest/f-json/get.shtml',
				data : { id: id },
				success : function(json) {
					if (json.code === 0) {
						var msg = '';
						if(JUtil.isEmpty(json.body.testResult)) {
							msg = '还没有执行测试';
						} else {
							msg = '测试时间：<span class="text-success">' + json.body.testTime + '</span><br/>'
								+ '测试结果：' + json.body.testResult;
						}
						alert(msg);
					}
					else if (json.code === -1)
						message(JUtil.msg.ajaxErr);
					else
						message(json.message);
				}
			});
		},
		del : function(id) {
			if(confirm('您确定要删除该记录吗?')) {
				JUtil.ajax({
					url : '${webroot}/prjApiTest/f-json/delete.shtml',
					data : { id: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		},
		//设置要测试的API
		setApi : function(id, name) {
			apiDtl.patId = id;
			apiDtl.name = name;
			apiDtl.loadInfo(1);
		}
};

var apiDtlPage = undefined;
var apiDtl = {
		//获取用户信息
		loadInfo : function(page) {
			if(apiDtl.patId != undefined) {
				$('#apiDtlInfo').css('display', 'block');
				$('#apiDtlInfoMsg').css('display', 'none');
			}
			$('#apiDtlInfoTitle').html('<span class="label label-warning">' + apiDtl.name + '</span>&nbsp;<small id="apiDtlResultPanel"></small>');
			if(!apiDtlPage) {
				apiDtlPage = new Page('apiDtlPage', apiDtl.loadInfo, 'apiDtlPanel', 'apiDtlPage');
				apiDtlPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                         '<th>路径</th>',
				                         '<th>状态</th>',
				                         '<th>测试结果</th>',
				                         '<th width="80">操作</th>',
				                         '</tr></thead><tbody>'].join('');
				apiDtlPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				apiDtlPage.page = page;

			JUtil.ajax({
				url : '${webroot}/prjApiTestDtl/f-json/pageQuery.shtml',
				data : { patId: apiDtl.patId, page:apiDtlPage.page, size:apiDtlPage.size },
				beforeSend: function(){ apiDtlPage.beforeSend('加载信息中...'); },
				error : function(json){ apiDtlPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							var _statusName = '';
							return ['<tr>',
							    	'<td><a href="',webroot,'/prjApi/f-view/dtl.shtml?prjId=',$('#prjId').val(),'&path=',obj.path,'" target="_blank" class="text-success">',obj.path,'</a></td>',
							    	'<td>',obj.statusName,'</td>',
							    	'<td>',obj.testResult,'</td>',
							    	'<td><a class="glyphicon glyphicon-edit text-success" href="javascript:apiDtl.edit(\'',obj.path,'\')" title="修改"></a>',
							    	'&nbsp; <a class="glyphicon glyphicon-remove text-success" href="javascript:apiDtl.del(\'',obj.path,'\')" title="删除"></a>',
							    	'</td>',
								'</tr>'].join('');
						}
						apiDtlPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
						
						if(json.body.total > 0) {
							JUtil.ajax({
								url : '${webroot}/prjApiTest/f-json/get.shtml',
								data : { id: apiDtl.patId },
								success : function(objJson) {
									if (objJson.code === 0) {
										var msg = '';
										if(JUtil.isEmpty(objJson.body.testResult)) {
											$('#apiDtlResultPanel').empty().append('加载中...');
										} else {
											$('#apiDtlResultPanel').empty().append('<a href="javascript:info.lookResult(\'' + apiDtl.patId + '\');" class="text-success">查看结果</a>');
										}
									}
									else if (json.code === -1)
										message(JUtil.msg.ajaxErr);
									else
										message(json.message);
								}
							});
						}
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		//编辑
		edit : function(path) {
			dialog({
				title: '编辑',
				url: webroot + '/prjApiTestDtl/f-view/edit.shtml?patId='+apiDtl.patId+'&path='+(path?path:''),
				type: 'iframe',
				width: 520,
				height: 450
			});
		},
		del : function(path) {
			if(confirm('您确定要删除该API接口吗?')) {
				JUtil.ajax({
					url : '${webroot}/prjApiTestDtl/f-json/delete.shtml',
					data : { patId: apiDtl.patId, path: path },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							apiDtl.loadInfo(1);
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		},
		test : function(path) {
			if(confirm('您确定全部测试吗?')) {
				JUtil.ajax({
					url : '${webroot}/prjApiTest/f-json/test.shtml',
					data : { id: apiDtl.patId },
					success : function(json) {
						if (json.code === 0) {
							alert(json.message);
							$('#refreshInterval').val('3');
							apiDtl.refreshChange();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		},
		refreshChange: function() {
			if(apiDtl.interval) {
				window.clearInterval(apiDtl.interval);
			}
			var _refresh = $('#refreshInterval').val();
			if(JUtil.isEmpty(_refresh)) {
				return;
			}
			apiDtl.interval = window.setInterval('apiDtl.loadInfo()', _refresh * 1000);
		}
};
$(function() {
	info.loadInfo(1);
	$('#prjId').change(function() {
		info.loadInfo(1);
	});
});
</script>
</body>
</html>