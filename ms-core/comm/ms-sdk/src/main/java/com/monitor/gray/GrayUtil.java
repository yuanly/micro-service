package com.monitor.gray;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.monitor.MonitorUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

public class GrayUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GrayUtil.class);
	
	private static Map<String, Gray> grayData = new HashMap<String, Gray>();

	/**
	 * 根据正常服务编号获取对象
	 * @param normalServiceId
	 * @return
	 */
	public static Gray get(String normalServiceId) {
		Gray gray = grayData.get(normalServiceId);
		return gray;
	}
	
	public static void sync() {
		long start = System.currentTimeMillis();
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		try {
			ResponseFrame frame = MonitorUtil.post("/api/msGray/findUse", paramsMap);
			if(ResponseCode.SUCC.getCode() == frame.getCode().intValue()) {
				List<Gray> data = frame.convertBody(Gray.class);
				//List<Map<String, Object>> data = frame.convertBody(HashMap.class);
				//List<Map<String, Object>> data = (List<Map<String, Object>>) frame.getBody();
				for (Gray gray : data) {
					//LOGGER.info(FrameJsonUtil.toString(gray));
					grayData.put(gray.getNormalServiceId(), gray);
				}
			}
			long diff = System.currentTimeMillis() - start;
			if(diff > 100) {
				LOGGER.info("更新灰度服务 - 耗时： " + diff + "ms");
			}
		} catch (IOException e) {
			LOGGER.error("获取灰度服务异常: " + e.getMessage());
		}
	}

}
