package com.module.admin.ms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.ms.dao.MsUserDao;
import com.module.admin.ms.pojo.MsUser;
import com.module.admin.ms.service.MsUserService;
import com.system.comm.model.KvEntity;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseFrame;
import com.system.handle.model.ResponseCode;

/**
 * ms_user的Service
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Component
public class MsUserServiceImpl implements MsUserService {

	@Autowired
	private MsUserDao msUserDao;
	
	@Override
	public ResponseFrame saveOrUpdate(MsUser msUser) {
		ResponseFrame frame = new ResponseFrame();
		MsUser org = get(msUser.getMuId());
		if(org == null) {
			msUserDao.save(msUser);
		} else {
			msUserDao.update(msUser);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MsUser get(String muId) {
		return msUserDao.get(muId);
	}

	@Override
	public ResponseFrame pageQuery(MsUser msUser) {
		msUser.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = msUserDao.findMsUserCount(msUser);
		List<MsUser> data = null;
		if(total > 0) {
			data = msUserDao.findMsUser(msUser);
		}
		Page<MsUser> page = new Page<MsUser>(msUser.getPage(), msUser.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String muId) {
		ResponseFrame frame = new ResponseFrame();
		msUserDao.delete(muId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<KvEntity> findSearch(String searchString) {
		List<KvEntity> list = msUserDao.findSearch(searchString);
		for (KvEntity kvEntity : list) {
			if (FrameStringUtil.isNotEmpty(kvEntity.getExp())) {
				kvEntity.setValue(kvEntity.getValue() + "<" + kvEntity.getCode() + ">");
			}
		}
		return list;
	}
}