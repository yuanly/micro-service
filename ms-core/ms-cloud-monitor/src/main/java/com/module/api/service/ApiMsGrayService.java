package com.module.api.service;

import org.springframework.stereotype.Component;

import com.system.handle.model.ResponseFrame;

@Component
public interface ApiMsGrayService {

	/**
	 * 获取所有使用中的灰度服务的列表
	 * @return
	 */
	public abstract ResponseFrame findUse();

}
