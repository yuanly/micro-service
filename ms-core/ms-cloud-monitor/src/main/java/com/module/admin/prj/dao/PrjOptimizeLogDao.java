package com.module.admin.prj.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.module.admin.prj.pojo.PrjOptimizeLog;

/**
 * prj_optimize_log的Dao
 * @author autoCode
 * @date 2018-05-31 15:51:51
 * @version V1.0.0
 */
public interface PrjOptimizeLogDao {

	public abstract void save(PrjOptimizeLog prjOptimizeLog);

	public abstract void update(PrjOptimizeLog prjOptimizeLog);

	public abstract void delete(@Param("id")Integer id);

	public abstract PrjOptimizeLog get(@Param("id")Integer id);

	public abstract List<PrjOptimizeLog> findPrjOptimizeLog(PrjOptimizeLog prjOptimizeLog);
	
	public abstract int findPrjOptimizeLogCount(PrjOptimizeLog prjOptimizeLog);

	public abstract void deleteAll();
}