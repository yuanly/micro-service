package com.module.admin.ms.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.module.admin.ms.pojo.MsGray;
import com.system.handle.model.ResponseFrame;

/**
 * ms_gray的Service
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Component
public interface MsGrayService {
	
	/**
	 * 保存或修改
	 * @param msGray
	 * @return
	 */
	public ResponseFrame saveOrUpdate(MsGray msGray);
	
	/**
	 * 根据mgId获取对象
	 * @param mgId
	 * @return
	 */
	public MsGray get(String mgId);

	/**
	 * 分页获取对象
	 * @param msGray
	 * @return
	 */
	public ResponseFrame pageQuery(MsGray msGray);
	
	/**
	 * 根据mgId删除对象
	 * @param mgId
	 * @return
	 */
	public ResponseFrame delete(String mgId);
	/**
	 * 获取所有使用的灰度服务
	 * @return
	 */
	public List<MsGray> findUse();
}