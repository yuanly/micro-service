package com.module.admin.ms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.admin.BaseController;
import com.module.admin.ms.pojo.MsUser;
import com.module.admin.ms.service.MsUserService;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 微服务用户的Controller
 * @author yuejing
 * @date 2016-10-20 17:55:37
 * @version V1.0.0
 */
@Controller
public class MsUserController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MsUserController.class);

	@Autowired
	private MsUserService msUserService;
	
	@RequestMapping(value = "/msUser/f-view/manager")
	public String manger(HttpServletRequest request) {
		return "admin/ms/msUser-manager";
	}
	
	@RequestMapping(value = "/msUser/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			MsUser msUser) {
		ResponseFrame frame = null;
		try {
			frame = msUserService.pageQuery(msUser);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	/**
	 * 跳转到管理页
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/msUser/f-view/edit")
	public String manger(HttpServletRequest request, ModelMap modelMap, String muId) {
		/*List<MsConfigValue> values = msConfigValueService.findByConfigId(configId);
		modelMap.put("values", values);
		List<MsConfig> configs = msConfigService.findAll();
		modelMap.put("configs", configs);*/
		MsUser msUser = msUserService.get(muId);
		modelMap.put("msUser", msUser);
		return "admin/ms/msUser-edit";
	}
	
	@RequestMapping(value = "/msUser/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response,
			MsUser msUser) {
		ResponseFrame frame = null;
		try {
			frame = msUserService.saveOrUpdate(msUser);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/msUser/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String muId) {
		ResponseFrame frame = null;
		try {
			frame = msUserService.delete(muId);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/msUser/f-json/findSearch")
	@ResponseBody
	public void findSearch(HttpServletRequest request, HttpServletResponse response, String searchString) {
		ResponseFrame frame = new ResponseFrame();
		try {
			List<KvEntity> data = msUserService.findSearch(searchString);
			frame.setBody(data);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}
}