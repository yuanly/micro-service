package com.module.admin.ms.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.module.admin.ms.pojo.MsGrayUser;

/**
 * ms_gray_user的Dao
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
public interface MsGrayUserDao {

	public abstract void save(MsGrayUser msGrayUser);

	public abstract void delete(@Param("mgId")String mgId, @Param("type")String type, @Param("typeValue")String typeValue);

	public abstract MsGrayUser get(@Param("mgId")String mgId, @Param("type")String type, @Param("typeValue")String typeValue);

	public abstract List<MsGrayUser> findMsGrayUser(MsGrayUser msGrayUser);
	
	public abstract int findMsGrayUserCount(MsGrayUser msGrayUser);

	public abstract List<MsGrayUser> findByMgId(@Param("mgId")String mgId);
}