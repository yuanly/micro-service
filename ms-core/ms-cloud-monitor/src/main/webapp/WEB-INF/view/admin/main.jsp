<%@page import="com.module.admin.prj.enums.PrjInfoStatus"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/inc/sys.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-${user.nickname}</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp"></jsp:include>
		<div class="c-right">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">欢迎您 <b><a href="${webroot}/sysUser/f-view/main.shtml">${user.nickname}</a></b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<a href="${webroot}/cliInfo/f-view/monitor.shtml" target="_blank" class="btn btn-default btn-sm">Agent监控</a>
							</div>
							<div class="btn-group jutil-btns" data-select="btn btn-success btn-sm" data-def="btn btn-default btn-sm">
						  		<a href="javascript:info.changeQuery('10');" class="btn btn-sm <c:choose><c:when test="${param.searchType==20}">btn-default</c:when><c:otherwise>btn-success</c:otherwise></c:choose>">我的收藏</a>
						  		<a href="javascript:info.changeQuery('20', 'filter');" class="btn btn-sm <c:choose><c:when test="${param.searchType==20}">btn-success</c:when><c:otherwise>btn-default</c:otherwise></c:choose>">所有</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div id="filterPanel" class="table-tool-panel" style="display: none;">
						<div class="row">
							<div class="col-sm-6">
								<span class="enter-panel">
									<input type="text" class="form-control input-sm w-120" id="name" placeholder="项目名称" value="${param.name}">
							  		<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
						  		</span>
							</div>
							<div class="col-sm-6 text-right">
							  	<div class="btn-group">
							  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增项目</a>
							  	</div>
							</div>
						</div>
				  	</div>
					<div class="row" id="infoPanel" style="min-height: 400px;padding: 10px 15px 0px 15px;"></div>
					<div id="infoPage" class="table-page-panel"></div>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		searchType: '10',
		init: function() {
			var searchType = '${param.searchType}';
			var sourceType = '';
			if (searchType == 20) {
				sourceType = 'filter';
			}
			if (JUtil.isEmpty(searchType)) {
				searchType = info.searchType;
			}
			info.changeQuery(searchType, sourceType);
			info.loadInfo(1);
		},
		changeQuery : function(searchType, sourceType) {
			info.searchType = searchType;
			info.loadInfo(1);
			var panel = $('#filterPanel');
			if (sourceType == 'filter') {
				// 显示筛选栏
				panel.css('display', 'block');
			} else {
				panel.css('display', 'none');
			}
		},
		//获取用户信息
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.size = 16;
			}
			if(page != undefined)
				infoPage.page = page;

			JUtil.ajax({
				url : '${webroot}/prjInfo/f-json/pageQuery.shtml',
				data : { page:infoPage.page, size:infoPage.size, name: $('#name').val(),searchType: info.searchType },
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							var _statusCls = '';
							if(obj.status === <%=PrjInfoStatus.NORMAL.getCode()%>) {
								_statusCls = ' class="text-success"';
							} else {
								_statusCls = ' class="text-danger"';
							}
							return ['<div class="col-sm-6 col-md-3" style="padding: 0px 10px;">',
								    '<div class="thumbnail">',
								    '  <div class="caption">',
								    '    <p><b>',obj.name,'</b> <small',_statusCls,'>(',obj.statusName,')</small></p>',
								    '    <p class="text-muted">',obj.code,'</p>',
								    '    <div><span class="btn-group"><a href="javascript:;" class="btn btn-success btn-xs" onclick="info.api(',obj.prjId,',\'',obj.name,'\')">API</a> ',
								    '		<a href="javascript:;" class="btn btn-warning btn-xs" onclick="info.version(',obj.prjId,',\'',obj.name,'\')">版本</a>',
								    '  </span>&nbsp; <a href="javascript:;" class="btn btn-default btn-xs" onclick="info.attPrj(',obj.prjId,')" title="收藏项目">',(info.searchType=='10'?'取消':''),'收藏</a>',
									'  <span class="dropdown opt-more">',
									'<a class="btn btn-default btn-xs dropdown-toggle" href="javascript:;" data-toggle="dropdown">更多...</a>',
									'<ul class="dropdown-menu" role="menu">',
									'<li role="presentation"><a href="javascript:info.config(',obj.prjId,',\'',obj.name,'\')" title="查看配置文件">查看配置</a></li>',
									'<li role="presentation"><a href="javascript:info.cache(',obj.prjId,',\'',obj.name,'\')">缓存管理</a></li>',
									'<li role="presentation"><a href="javascript:info.gray(',obj.prjId,',\'',obj.name,'\')">灰度服务</a></li>',
									'<li role="presentation"><a href="javascript:info.monitor(',obj.prjId,',\'',obj.name,'\')" title="查看项目的监控">查看监控</a></li>',
									'<li role="presentation"><a href="javascript:info.ant(',obj.prjId,',\'',obj.name,'\')" title="查看原型">原型管理</a></li>',
									'<li role="presentation"><a href="javascript:info.ds(',obj.prjId,',\'',obj.name,'\')">数据源</a></li>',
									'<li role="presentation"><a href="javascript:info.log(',obj.prjId,',\'',obj.name,'\')" title="查看项目的链路跟踪日志">链路日志</a></li>',
									'<li role="presentation"><a href="javascript:info.optimizeLog(',obj.prjId,',\'',obj.name,'\')">系统优化</a></li>',
									'<li role="presentation"><a href="javascript:info.autoCode(',obj.prjId,',\'',obj.name,'\')">生成源码</a></li>',
									'<li role="presentation"><a href="javascript:info.edit(',obj.prjId,')">修改</a></li>',
									'<li role="presentation"><a href="javascript:info.del(',obj.prjId,')">删除</a></li>',
									'</ul>',
									'</span>',
									'</div></div>',
								    '</div>',
								  '</div>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
						JUtil.sys.initOptMore();
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		//编辑项目
		edit : function(id) {
			dialog({
				title: '编辑项目',
				url: webroot + '/prjInfo/f-view/edit.shtml?prjId='+(id?id:''),
				type: 'iframe',
				width: 620,
				height: 555
			});
		},
		//添加到收藏
		attPrj: function(prjId) {
			if(info.searchType=='10') {
				JUtil.ajax({
					url : '${webroot}/sysUserAtt/f-json/delete.shtml',
					data : { type: '10', typeNo: prjId },
					success : function(json) {
						if (json.code === 0) {
							message('取消收藏成功');
							info.loadInfo();
						}
						else if (json.code === -1) message(JUtil.msg.ajaxErr);
						else message(json.message);
					}
				});
			} else {
				JUtil.ajax({
					url : '${webroot}/sysUserAtt/f-json/save.shtml',
					data : { type: '10', typeNo: prjId },
					success : function(json) {
						if (json.code === 0) {
							message('收藏成功');
							info.loadInfo();
						}
						else if (json.code === -1) message(JUtil.msg.ajaxErr);
						else message(json.message);
					}
				});
			}
		},
		del : function(id) {
			if(confirm('您确定要删除该项目吗?')) {
				JUtil.ajax({
					url : '${webroot}/prjInfo/f-json/delete.shtml',
					data : { prjId: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo(1);
						}
						else if (json.code === -1) message(JUtil.msg.ajaxErr);
						else message(json.message);
					}
				});
			}
		},
		//版本管理
		version : function(prjId, name) {
			location = '${webroot}/prjVersion/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/prjVersion/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//查看项目的api信息
		api : function(prjId, name) {
			location = '${webroot}/prjApi/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/prjApi/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//查看配置文件
		config : function(prjId, name) {
			location = '${webroot}/msConfig/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/msConfig/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//查看原型
		ant : function(prjId, name) {
			location = '${webroot}/prjAnt/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/prjAnt/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//查看监控
		monitor : function(prjId, name) {
			location = '${webroot}/prjMonitor/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/prjMonitor/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//数据源
		ds : function(prjId, name) {
			location = '${webroot}/prjDs/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/prjDs/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//查看日志
		log : function(prjId, name) {
			location = '${webroot}/logInfo/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/logInfo/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//缓存管理
		cache: function(prjId, name) {
			location = '${webroot}/cacheInfo/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/cacheInfo/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//系统优化
		optimizeLog : function(prjId, name) {
			location = '${webroot}/prjOptimizeLog/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/prjOptimizeLog/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//生成源码
		autoCode : function(prjId, name) {
			location = '${webroot}/codePrj/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/codePrj/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		},
		//灰度服务
		gray : function(prjId, name) {
			location = '${webroot}/msGray/f-view/manager.shtml?searchType='+info.searchType+'&prjId=' + prjId + '&name=' + name;
			//window.open('${webroot}/codePrj/f-view/manager.shtml?prjId=' + prjId + '&name=' + name);
		}
};
$(function() {
	info.init();
});
</script>
</body>
</html>