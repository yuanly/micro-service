package com.module.admin.ms.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.module.admin.ms.pojo.MsGrayUser;
import com.system.handle.model.ResponseFrame;

/**
 * ms_gray_user的Service
 * @author autoCode
 * @date 2019-04-02 10:54:54
 * @version V1.0.0
 */
@Component
public interface MsGrayUserService {
	
	/**
	 * 保存
	 * @param msGrayUser
	 * @return
	 */
	public ResponseFrame save(MsGrayUser msGrayUser);
	
	/**
	 * 根据mgId获取对象
	 * @param mgId
	 * @return
	 */
	public MsGrayUser get(String mgId, String type, String typeValue);

	/**
	 * 分页获取对象
	 * @param msGrayUser
	 * @return
	 */
	public ResponseFrame pageQuery(MsGrayUser msGrayUser);
	
	/**
	 * 根据mgId删除对象
	 * @param mgId
	 * @param typeValue 
	 * @param type 
	 * @return
	 */
	public ResponseFrame delete(String mgId, String type, String typeValue);

	public List<MsGrayUser> findByMgId(String mgId);
}