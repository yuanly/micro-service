package com.module.admin.ms.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.admin.BaseController;
import com.module.admin.ms.pojo.MsGray;
import com.module.admin.ms.pojo.MsGrayUser;
import com.module.admin.ms.service.MsGrayService;
import com.module.admin.ms.service.MsGrayUserService;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 微服务用户的Controller
 * @author yuejing
 * @date 2016-10-20 17:55:37
 * @version V1.0.0
 */
@Controller
public class MsGrayController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MsGrayController.class);

	@Autowired
	private MsGrayService msGrayService;
	@Autowired
	private MsGrayUserService msGrayUserService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(FrameTimeUtil.FMT_DEFAULT);
			dateFormat.setLenient(false);
			//true:允许输入空值，false:不能为空值
			binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		}catch(Exception e) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(FrameTimeUtil.FMT_YYYY_MM_DD);
			dateFormat.setLenient(false);
			//true:允许输入空值，false:不能为空值
			binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		}
	}
	@RequestMapping(value = "/msGray/f-view/manager")
	public String manger(HttpServletRequest request) {
		return "admin/ms/msGray-manager";
	}
	
	@RequestMapping(value = "/msGray/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			MsGray msGray) {
		ResponseFrame frame = null;
		try {
			frame = msGrayService.pageQuery(msGray);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/msGray/f-view/edit")
	public String manger(HttpServletRequest request, ModelMap modelMap, String mgId) {
		MsGray msGray = msGrayService.get(mgId);
		modelMap.put("msGray", msGray);
		return "admin/ms/msGray-edit";
	}
	
	@RequestMapping(value = "/msGray/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response,
			MsGray msGray) {
		ResponseFrame frame = null;
		try {
			frame = msGrayService.saveOrUpdate(msGray);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/msGray/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String mgId) {
		ResponseFrame frame = null;
		try {
			frame = msGrayService.delete(mgId);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/msGray/f-json/pageQueryUser")
	@ResponseBody
	public void pageQueryUser(HttpServletRequest request, HttpServletResponse response,
			MsGrayUser msGrayUser) {
		ResponseFrame frame = null;
		try {
			frame = msGrayUserService.pageQuery(msGrayUser);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	@RequestMapping(value = "/msGray/f-view/editUser")
	public String editUser(HttpServletRequest request, ModelMap modelMap, String mgId) {
		MsGray msGray = msGrayService.get(mgId);
		modelMap.put("msGray", msGray);
		return "admin/ms/msGray-editUser";
	}
	@RequestMapping(value = "/msGray/f-json/saveUser")
	@ResponseBody
	public void saveUser(HttpServletRequest request, HttpServletResponse response,
			MsGrayUser msGrayUser) {
		ResponseFrame frame = null;
		try {
			frame = msGrayUserService.save(msGrayUser);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	@RequestMapping(value = "/msGray/f-json/deleteUser")
	@ResponseBody
	public void deleteUser(HttpServletRequest request, HttpServletResponse response,
			String mgId, String type, String typeValue) {
		ResponseFrame frame = null;
		try {
			frame = msGrayUserService.delete(mgId, type, typeValue);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}
}