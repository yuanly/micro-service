<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld" %>
<div class="c-left left-menu">
	<div class="panel-group">
		<div class="panel panel-info top">
			<div class="panel-heading">
				<h4 class="panel-title text-center">
					<a href="${webroot}/sysUser/f-view/main.shtml"><span class="glyphicon glyphicon-home"></span> 个人中心</a>
				</h4>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'prj'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapsePrj"><span class="glyphicon glyphicon-folder-close"></span> 项目信息 <span class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapsePrj" class="panel-collapse collapse <c:if test="${param.first == 'prj'}">in</c:if>">
				<div class="panel-body">
					<%-- <div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'prjInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/prjInfo/f-view/manager.shtml">项目管理</a>
					</div> --%>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'prjMonitorManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/prjMonitor/f-view/manager.shtml">服务监控</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'logInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/logInfo/f-view/manager.shtml">链路日志</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'cacheInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/cacheInfo/f-view/manager.shtml">缓存管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'prjOptimizeLogManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/prjOptimizeLog/f-view/manager.shtml">系统优化</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'prjAntManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/prjAnt/f-view/manager.shtml">原型管理</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'ms'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapseMs"><span class="glyphicon glyphicon-th"></span> 微服务<span
						class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapseMs" class="panel-collapse collapse <c:if test="${param.first == 'ms'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'msConfigManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/msConfig/f-view/manager.shtml">配置文件管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'msSecretManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/msSecret/f-view/manager.shtml">密钥管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'msUserManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/msUser/f-view/manager.shtml">微服务用户</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'msGrayManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/msGray/f-view/manager.shtml">灰度服务</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'cli'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapseCli"><span class="glyphicon glyphicon-eur"></span> Agent
						<span class="caret"></span>
					</a>
				</h4>
			</div>
			<div id="mlCollapseCli" class="panel-collapse collapse <c:if test="${param.first == 'cli'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'cliInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/cliInfo/f-view/manager.shtml">Agent管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'cliInfoMonitor'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/cliInfo/f-view/monitor.shtml">Agent监控</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'tts'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapseTask"><span class="glyphicon glyphicon-time"></span> 定时任务<span
						class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapseTask" class="panel-collapse collapse <c:if test="${param.first == 'tts'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'servInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/ttsServInfo/f-view/manager.shtml">服务管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'servInfoChart'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/ttsServInfo/f-view/chart.shtml">服务图表</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'projectManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/ttsTaskProject/f-view/manager.shtml">项目管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'projectChart'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/ttsTaskProject/f-view/chart.shtml">项目图表</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'projectJobLog'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/ttsTaskJobLog/f-view/manager.shtml">异常调度</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'configManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/ttsSysConfig/f-view/manager.shtml">参数配置</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default bottom">
			<div class="panel-heading <c:if test="${param.first == 'sys'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapseSys"><span class="glyphicon glyphicon-cog"></span> 系统管理 <span
						class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapseSys" class="panel-collapse collapse <c:if test="${param.first == 'sys'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'sysUserManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block"
							href="${webroot}/sysUser/f-view/manager.shtml">用户管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'sysConfigManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block"
							href="${webroot}/sysConfig/f-view/manager.shtml">系统配置</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>