package com.module.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.module.api.service.ApiMsGrayService;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 灰度服务的接口
 * @author yuejing
 * @date 2019年1月29日 下午9:31:59
 * @version V1.0.0
 */
@RestController
public class ApiMsGrayController {

    private final Logger LOGGER = LoggerFactory.getLogger(ApiMsGrayController.class);
    
	@Autowired
	private ApiMsGrayService msGrayService;

	/**
	 * 获取所有使用中灰度服务的列表
	 * @return
	 */
	@RequestMapping(value = "/api/msGray/findUse")
	public ResponseFrame findUse() {
		try {
			ResponseFrame frame = msGrayService.findUse();
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}
	
	/**
	 * 获取所有服务
	 * @return
	 */
	/*@RequestMapping(value = "/api/msGray/services")
	public ResponseFrame serviceList() {
		try {
			ResponseFrame frame = new ResponseFrame();
			frame.setBody(serviceService.services());
			frame.setCode(ResponseCode.SUCC.getCode());
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}*/

}