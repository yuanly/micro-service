package com.frame.test.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.test.pojo.Test;
import com.frame.test.service.TestService;
import com.monitor.rest.RestUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.system.comm.service.BaseService;
import com.system.comm.utils.FrameJsonUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

@Component
public class TestServiceImpl extends BaseService implements TestService {

	@Autowired
	private RestUtil restUtil;
	
	@Override
	@HystrixCommand(fallbackMethod = "errorServiceFallbackGet")
	public String get(String id, String msGuId) {
		//通过restTemplate调用不通过才触发Hystrix
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		params.put("msGuId", msGuId);
		ResponseFrame res = restUtil.request("biz-service-api", "/test/get", params, Test.class);
		return "get: " + FrameJsonUtil.toString(res.getBody());
	}

	@Override
	@HystrixCommand(fallbackMethod = "errorServiceFallbackFindAll")
	public List<Test> findAll() {
		Map<String, Object> params = new HashMap<String, Object>();
		ResponseFrame res = restUtil.request("biz-service-api", "/test/findAll", params, Test.class);
		return res.getBody();
	}
	
	/**
	 * 异常服务的回调
	 * @return
	 */
	public String errorServiceFallbackGet(String id, String msGuId) {
		ResponseFrame frame = new ResponseFrame();
		frame.setCode(ResponseCode.SERVER_ERROR_FALLBACK.getCode());
		frame.setMessage(ResponseCode.SERVER_ERROR_FALLBACK.getMessage());
        return FrameJsonUtil.toString(frame);
    }
	/**
	 * 异常服务的回调
	 * @return
	 */
	public List<Test> errorServiceFallbackFindAll() {
		/*ResponseFrame frame = new ResponseFrame();
		frame.setCode(ResponseCode.SERVER_ERROR_FALLBACK.getCode());
		frame.setMessage(ResponseCode.SERVER_ERROR_FALLBACK.getMessage());*/
        return new ArrayList<Test>();
    }
}
