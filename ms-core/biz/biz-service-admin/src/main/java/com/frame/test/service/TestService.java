package com.frame.test.service;

import java.util.List;

import com.frame.test.pojo.Test;

public interface TestService {

	public String get(String id, String msGuId);

	public List<Test> findAll();

}
